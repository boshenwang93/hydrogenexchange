#! /usr/bin/python3

import os

# iterating the pdb folder
pdb_directory = "/home/bwang/project/hydrogenexchange/EF/pdb/"

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        file_path = subdir + file
        command_line = "python EFoldMine.py \t" + file_path + "\t -o " + file +".out" 
        os.system(command_line)

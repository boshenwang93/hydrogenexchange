#! /usr/bin/python3

import re 

full_residue = "/home/bwang/project/hydrogenexchange/EF/unique_ef_balance.csv"
list_residue_info = []

f = open(full_residue,"r")
for line in f:
    line = line.strip()
    exp_id = pdb = re.split(",",line)[0]
    pdb = re.split(",",line)[1]
    residue =  re.split(",",line)[2]
    seqnum =  re.split(",",line)[3]

    residue_info = exp_id + "," + pdb + "," + residue + "," + seqnum
    list_residue_info.append(residue_info)
f.close()


svm_file = "/home/bwang/project/hydrogenexchange/EF/Efoldds.csv"

list_out = []
f1 = open(svm_file,"r")
for line in f1:
    line  = line.strip()
    exp_id = pdb = re.split(",",line)[0]
    pdb = re.split(",",line)[1]
    residue =  re.split(",",line)[2]
    seqnum =  re.split(",",line)[3]

    svm_residue_info = exp_id + "," + pdb + "," + residue + "," + seqnum

    if svm_residue_info in list_residue_info:
        if line not in list_out:
            list_out.append(line) 
f1.close()

for e in list_out:
    print(e)
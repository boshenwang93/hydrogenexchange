#! /usr/bin/python3

import re 

full_residue = "/home/bwang/project/hydrogenexchange/machinelearning/negative.csv"
list_residue_info = []

f = open(full_residue,"r")
for line in f:
    pdb = re.split(",",line)[1]
    residue =  re.split(",",line)[2]
    seqnum =  re.split(",",line)[3]

    residue_info = pdb + "," + residue + "," + seqnum
    if residue_info not in list_residue_info:
        list_residue_info.append(residue_info)

f.close()


svm_file = "/home/bwang/project/hydrogenexchange/EF/intermediate/.csv"

f1 = open(svm_file,"r")
for line in f1:
    line  = line.strip()
    pdb = re.split(",",line)[0]
    residue =  re.split(",",line)[1]
    seqnum =  re.split(",",line)[2]

    svm_residue_info = pdb + "," + residue + "," + seqnum

    if svm_residue_info in list_residue_info:
        print(line)
f1.close()
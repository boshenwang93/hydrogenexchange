#! /usr/bin/python3
import re

list_ef = [] 
ef = open("/home/bwang/project/hydrogenexchange/EF/EF_site_NMR.csv",'r')
for line in ef:
    line = line.strip()
    pdb_id = re.split(",",line)[0]
    residue = re.split(",",line)[3]
    index = re.split(",",line)[4]
    index = int(index)

    if pdb_id == "1hrh":
        index = int(index) + 426
    elif pdb_id == "1hfz":
        index = index - 1
    elif pdb_id == "1e3y":
        index = index + 88
    elif pdb_id == "1a64":
        if index >= 46:
            index = index + 2

    out = pdb_id  + "," + residue + "," + str(index)

    if out not in list_ef:
        list_ef.append(out)
ef.close()

for e in list_ef:
    print(e)
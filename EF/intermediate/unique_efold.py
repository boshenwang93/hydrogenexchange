#! /usr/bin/python3

import re 

full_residue = "/home/bwang/project/hydrogenexchange/EF/Efoldds.csv"
list_residue_info = []

f = open(full_residue,"r")
for line in f:
    line = line.strip()
    exp_id =  re.split(",",line)[0]
    pdb = re.split(",",line)[1]
    residue =  re.split(",",line)[2]
    seqnum =  re.split(",",line)[3]

    residue_info = exp_id + "," + pdb + "," + residue + "," + seqnum
    if residue_info not in list_residue_info:
        list_residue_info.append(residue_info)
f.close()


our_db = "/home/bwang/project/hydrogenexchange/EF/our_ef.csv"

for e in list_residue_info:
    index = 0

    f1 = open(our_db,"r")
    for line in f1:
         line = line.strip()
         exp_id =  re.split(",",line)[0]
         pdb = re.split(",",line)[1]
         residue =  re.split(",",line)[2]
         seqnum =  re.split(",",line)[3]

         residue_info = exp_id + "," + pdb + "," + residue + "," + seqnum

         if e == residue_info:
             index += 1 
             if index == 1:
                 print(line)
    f1.close()
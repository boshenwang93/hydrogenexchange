#! /usr/bin/python3
import re
import os


def check_pdb(pdb_file_path):

    # list_residue
    list_residue = []

    f = open(pdb_file_path, 'r')
    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            residue_identify = residue + "," + residue_seq_number + "," + chain
            if residue_identify not in list_residue:
                list_residue.append(residue_identify)
    f.close()
    
    return list_residue



## iterating the catsp folder ########
pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/"

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        file_path = subdir + file

        print("###############################")
        print(file_path)
        list_residue = check_pdb(file_path)
        for e in list_residue:
            print(e)


# check the pdb which contains multiple conformations
# return count of conformations
# check whether conformations contains same atoms

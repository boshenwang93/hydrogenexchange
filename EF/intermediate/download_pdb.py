#! /usr/bin/python3
import os

list_pdb = [
    "1hce",
    "1am7",
    "1f21",
    "1e3y",
    "2crt",
    "1rbx",
    "1pga",
    "1coe",
    "9pcy",
    "1onc",
    "2vil",
    "1omu",
    "5dfr",
    "1bdd",
    "1hrh",
    "1lz1",
    "1yob",
    "1joo",
    "1hel",
    "2abd",
    "2ptl",
    "1ubq",
    "2eql",
    "1hfz",
    "2lzm",
    "5pti",
    "1a64",
    "1i1b",
    "1mbc",
]

for pdb_id in list_pdb:
    pdb_id = pdb_id.strip()
    rcsb_link = "https://www.rcsb.org/pdb/download/downloadFastaFiles.do?structureIdList=" + pdb_id + "&compressionType=uncompressed"
    command_text = "wget \t" + "\"" + rcsb_link + "\"" + " -P /home/bwang/project/hydrogenexchange/EF/pdb/"
    os.system(command_text)
#! /usr/bin/python3
import re 
import os 

dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V",
}


def readEFout(file_path_EFout):
    list_output = []
    
    f = open(file_path_EFout,"r")

    for line in f:
        if line[0].isalpha():
            line = line.strip()

            residue_one = re.split("\s+",line)[0]
            svm_score = re.split("\s+",line)[1]
            out = residue_one + "," + svm_score
            list_output.append(out)
    f.close()

    return list_output


EFout_directory = "/home/bwang/project/hydrogenexchange/EF/EFout/"

for subdir, dirs, files in os.walk(EFout_directory):
    for file in files:
        file_path = subdir + file

        pdb_id = re.split(".out",file)[0]
        pdb_id = pdb_id.strip()

        list_residue = readEFout(file_path)

        for e in list_residue:
            index = list_residue.index(e) + 1
            if pdb_id == "1hrh":
                index = int(index) + 426
            elif pdb_id == "1hfz":
                index = index - 1
            elif pdb_id == "1e3y":
                index = index + 88
            elif pdb_id == "1a64":
                if index >= 46:
                    index = index + 2

            score = re.split(",",e)[1]

            residue_type = ''
            residue_one = re.split(",",e)[0]
            for k,v in dic_tri_single.items():
                if v == residue_one:
                    residue_type += k

                    out = pdb_id + "," + residue_type + "," + str(index) + "," + score
                    print(out)
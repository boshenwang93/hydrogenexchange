#! /usr/bin/python3

import re 

dic_pdb_exp = {}

exp_file  = "/home/bwang/project/hydrogenexchange/EF/intermediate/EF_site_NMR.csv"

f = open(exp_file,"r")
for line in f:
    line = line.strip()
    pdb = re.split(",",line)[0]
    exp_id = re.split(",",line)[1]

    dic_pdb_exp[pdb] = exp_id
f.close()

ef_ds = "/home/bwang/project/hydrogenexchange/EF/intermediate/negative.csv"

f1 = open(ef_ds,"r")

for line in f1:
    line = line.strip()
    pdb = re.split(",",line)[0]
    out = dic_pdb_exp[pdb] + "," + line
    print(out)
    
f1.close()
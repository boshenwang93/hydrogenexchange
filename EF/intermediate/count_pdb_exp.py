#! /usr/bin/python3
import re

list_pdb = []
list_exp = []
list_unique = []

file = "EF_site_NMR.csv"

f = open(file,"r")

for line in f :
    pdb = re.split(",",line)[0]
    exp = re.split(",",line)[1]
    residue =  re.split(",",line)[3]
    seqnum =  re.split(",",line)[4]

    if pdb not in list_pdb:
        list_pdb.append(pdb)

    if exp not in list_exp:
        list_exp.append(exp)
    
    unique = pdb + residue + seqnum
    if unique not in list_unique:
        list_unique.append(unique)

f.close()

# print(len(list_exp))
# print(len(list_pdb))
# print(len(list_unique))

for e in list_pdb:
    print(e)
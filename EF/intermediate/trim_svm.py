#! /usr/bin/python3
import re 

### according to svm_ef svm_nonex to hook the rf_nd/pd

svm_file = "rf_nd.csv"

list_id = []
f1 = open(svm_file,"r")
for line in f1:
    line = line.strip()
    dsid = re.split(",",line)[0]
    pdb = re.split(",",line)[1]
    residue = re.split(",",line)[2]
    seqnum = re.split(",",line)[3]
    out = dsid + "," + pdb + "," + residue + "," + seqnum

    if out not in list_id:
        list_id.append(out)
f1.close()


db_file = "svm_nonex.csv"

for e in list_id:
    count = 0 

    f2 = open(db_file,"r")
    for line in f2:
        line = line.strip()
        dsid = re.split(",",line)[0]
        pdb = re.split(",",line)[1]
        residue = re.split(",",line)[2]
        seqnum = re.split(",",line)[3]

        out = dsid + "," + pdb + "," + residue + "," + seqnum

        if out == e:
            count +=1
            if count ==1:
                print(line)
    f2.close()

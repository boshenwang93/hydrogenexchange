
#! /usr/bin/python3
import re 
import math 

dic_1 = {}
dic_2 = {}
dic_3 = {}
dic_4 = {}
dic_5 = {}

list_unique_fpr = []

f1 = open("1.csv","r")
for line in f1:
    line = line.strip()
    fpr = float( re.split(",",line)[0])
    sen = float( re.split(",",line)[1])
    dic_1[fpr] = sen
    if fpr not in list_unique_fpr:
        list_unique_fpr.append(fpr)
f1.close()

f2 = open("2.csv","r")
for line in f2:
    line = line.strip()
    fpr = float( re.split(",",line)[0])
    sen = float( re.split(",",line)[1])
    dic_2[fpr] = sen
    if fpr not in list_unique_fpr:
        list_unique_fpr.append(fpr)
f2.close()


f3 = open("3.csv","r")
for line in f3:
    line = line.strip()
    fpr = float( re.split(",",line)[0])
    sen = float( re.split(",",line)[1])
    dic_3[fpr] = sen
    if fpr not in list_unique_fpr:
        list_unique_fpr.append(fpr)
f3.close()


f4 = open("4.csv","r")
for line in f4:
    line = line.strip()
    fpr = float( re.split(",",line)[0])
    sen = float( re.split(",",line)[1])
    dic_4[fpr] = sen
    if fpr not in list_unique_fpr:
        list_unique_fpr.append(fpr)
f4.close()


f5 = open("5.csv","r")
for line in f5:
    line = line.strip()
    fpr = float( re.split(",",line)[0])
    sen = float( re.split(",",line)[1])
    dic_5[fpr] = sen
    if fpr not in list_unique_fpr:
        list_unique_fpr.append(fpr)
f5.close()



for single_fpr in list_unique_fpr:
    count = 0
    total_sen = 0
    if single_fpr in dic_1:
        count += 1
        total_sen += dic_1[single_fpr]
    if single_fpr in dic_2:
        count += 1
        total_sen += dic_2[single_fpr]
    if single_fpr in dic_3:
        count += 1
        total_sen += dic_3[single_fpr]
    if single_fpr in dic_4:
        count += 1
        total_sen += dic_4[single_fpr]
    if single_fpr in dic_5:
        count += 1
        total_sen += dic_5[single_fpr]
    
    avg_sen = total_sen / count
    v_square = 0
    if single_fpr in dic_1:
        sen = dic_1[single_fpr]
        v_square += math.pow ( (avg_sen - sen) ,2 )
    if single_fpr in dic_2:
        sen = dic_2[single_fpr]
        v_square += math.pow ( (avg_sen - sen) ,2 )
    if single_fpr in dic_3:
        sen = dic_3[single_fpr]
        v_square += math.pow ( (avg_sen - sen) ,2 )
    if single_fpr in dic_4:
        sen = dic_4[single_fpr]
        v_square += math.pow ( (avg_sen - sen) ,2 )
    if single_fpr in dic_5:
        sen = dic_5[single_fpr]
        v_square += math.pow ( (avg_sen - sen) ,2 )
    
    sd = math.sqrt( v_square / count )
    lower = avg_sen - sd 
    upper = avg_sen + sd

    outline = str(single_fpr) + "," + str(avg_sen) + "," + str(lower) + "," + str(upper)
    print(outline)



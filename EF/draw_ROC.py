#! /usr/bin/python3
import re
import matplotlib.pyplot as plt

RF_file = "data_roc.csv"
list_rf_fpr = []
list_rf_sen = []
list_upper = []
list_lower = [] 

f = open(RF_file,"r")
for line in f:
    line = line.strip()
    fpr = re.split(",",line)[0]
    sen = re.split(",",line)[1]
    fpr = float(fpr)
    sen = float(sen)
    list_rf_fpr.append(fpr)
    list_rf_sen.append(sen)
    list_lower.append(float(re.split(",",line)[2]))
    list_upper.append(float(re.split(",",line)[3]))
f.close()

plt.fill_between(list_rf_fpr, list_lower, list_upper, color=(0.85, 0.85, 0.85) )


##############################################################
######### 10-fold ROC ####################################

def auc(list_pos_score, list_neg_score, positive_label_count, negative_label_count):
    list_sensitivity = []
    list_fpr = []

    ######  ROC curve
    i = 0
    while i <= 1:
        i += 0.001
        count_tp = 0 
        for p in list_pos_score:
            if p > i:
                count_tp += 1
    
        count_tn = 0
        for n in list_neg_score:
            if n < i:
                count_tn += 1
    
        sensititivity = count_tp / positive_label_count
        fpr = 1 - count_tn / negative_label_count
        sensititivity = "%.3f"% sensititivity
        fpr  = "%.3f"% fpr
    # out = str(i) + "," + str(sensititivity) + "," + str(fpr)
        list_sensitivity.append(sensititivity)
        list_fpr.append(fpr)

    auc = 0.0 
    for j in range(0,999,1):
        dx =  -float(list_fpr[j+1]) + float(list_fpr[j]) 
        dy = (float(list_sensitivity[j+1]) + float(list_sensitivity[j]))/2
        area = dx * dy 
        auc += area
    # print("%.3f"% auc)

    return auc, list_sensitivity, list_fpr
list_pos_score = []
list_neg_score = []

positive_ds = "5fold_result"
f1 = open(positive_ds,"r")

for line in f1:
    line = line.strip()
    index = re.split("\s+",line)[0]
    index = int(index)
    score = re.split("\s+",line)[2]
    score = float(score)
    if index > 327:
        list_pos_score.append(score)
    else:
        list_neg_score.append(score)
f1.close()

total_auc = 0.0
for i in range(1,6,1):
    begin_index = int (65.2 * i - 65.2)
    end_index = int (65.1 * i - 1) 

    sub_pos_list = []
    sub_neg_list = []

    for j in range(begin_index, end_index+1, 1):
        sub_neg_list.append( list_neg_score[j])
        sub_pos_list.append( list_pos_score[j])
    
    positive_count = len(sub_pos_list)
    negative_count = len(sub_neg_list)
    area, l_sen, l_fpr = auc(sub_pos_list, sub_neg_list, positive_count, negative_count)
    plt.plot(l_fpr, l_sen, color =(0.55, 0.55, 0.55)  , linewidth = 0.5)




plt.plot(list_rf_fpr,list_rf_sen, color = "blue", linewidth = 2, alpha = 2)






#######################################################
########## SVM ROC ####################################

list_pos_score = []
positive_ds = "svm_pd.csv"
f1 = open(positive_ds,"r")

for line in f1:
    line = line.strip()
    score = re.split(",",line)[4]
    score = float(score)
    list_pos_score.append(score)
f1.close()

list_neg_score = []
negative_ds = "svm_nd.csv"
f2 = open(negative_ds,"r")
for line in f2:
    line = line.strip()
    score = re.split(",",line)[4]
    score = float(score)
    list_neg_score.append(score)
f2.close()

list_sensitivity = []
list_fpr = []

######  ROC curve
i = 0
while i <= 1:
    i += 0.001
    count_tp = 0 
    for p in list_pos_score:
        if p > i:
            count_tp += 1
    
    count_tn = 0
    for n in list_neg_score:
        if n < i:
            count_tn += 1
    
    sensititivity = count_tp / 326
    fpr = 1 - count_tn / 326

    sensititivity = "%.3f"% sensititivity
    fpr  = "%.3f"% fpr

    # out = str(i) + "," + str(sensititivity) + "," + str(fpr)

    list_sensitivity.append(sensititivity)
    list_fpr.append(fpr)


plt.plot(list_fpr, list_sensitivity, color = "red",   linewidth = 1 )

plt.plot([0,1],[0,1], "--", color = "black",  linewidth=1)


plt.axes().set_aspect('equal')
plt.show()
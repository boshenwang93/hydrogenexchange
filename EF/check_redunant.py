#! /usr/bin/python3

import re 

full_residue = "rf_ef.csv"
list_residue_info = []

f = open(full_residue,"r")
for line in f:
    exp_id = re.split(",",line)[0]
    pdb = re.split(",",line)[1]
    residue =  re.split(",",line)[2]
    seqnum =  re.split(",",line)[3]

    residue_info =exp_id + "," + pdb + "," + residue + "," + seqnum
    if residue_info not in list_residue_info:
        list_residue_info.append(residue_info)

f.close()

print(len (list_residue_info))
#! /usr/bin/python3

import re
import math 

list_pos_score = []
positive_ds = "svm_pd.csv"
f1 = open(positive_ds,"r")

for line in f1:
    line = line.strip()
    score = re.split(",",line)[4]
    score = float(score)
    list_pos_score.append(score)
f1.close()

list_neg_score = []
negative_ds = "svm_nd.csv"
f2 = open(negative_ds,"r")
for line in f2:
    line = line.strip()
    score = re.split(",",line)[4]
    score = float(score)
    list_neg_score.append(score)
f2.close()

list_sensitivity = []
list_fpr = []

######  ROC curve
i = 0
while i <= 1:
    i += 0.001
    count_tp = 0 
    for p in list_pos_score:
        if p > i:
            count_tp += 1
    
    count_tn = 0
    for n in list_neg_score:
        if n < i:
            count_tn += 1
    
    sensititivity = count_tp / 326
    fpr = 1 - count_tn / 326

    sensititivity = "%.3f"% sensititivity
    fpr  = "%.3f"% fpr

    # out = str(i) + "," + str(sensititivity) + "," + str(fpr)

    list_sensitivity.append(sensititivity)
    list_fpr.append(fpr)

auc = 0.0 
for j in range(0,999,1):
    dx =  -float(list_fpr[j+1]) + float(list_fpr[j]) 
    dy = (float(list_sensitivity[j+1]) + float(list_sensitivity[j]))/2
    area = dx * dy 
    auc += area
print(auc)

## draw the figure
import matplotlib.pyplot as plt
plt.plot(list_fpr, list_sensitivity)


plt.title("ROC Curve")
plt.xlabel("False Positive Rate")
plt.ylabel("True Positive Rate")

plt.show()
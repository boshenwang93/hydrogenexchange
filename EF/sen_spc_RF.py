#! /usr/bin/python3

import re
import matplotlib.pyplot as plt
import math


def auc(list_pos_score, list_neg_score, positive_label_count, negative_label_count):
    list_sensitivity = []
    list_fpr = []

    ######  ROC curve
    i = 0
    while i <= 1:
        i += 0.001
        count_tp = 0 
        for p in list_pos_score:
            if p > i:
                count_tp += 1
    
        count_tn = 0
        for n in list_neg_score:
            if n < i:
                count_tn += 1
    
        sensititivity = count_tp / positive_label_count
        fpr = 1 - count_tn / negative_label_count
        sensititivity = "%.3f"% sensititivity
        fpr  = "%.3f"% fpr
    # out = str(i) + "," + str(sensititivity) + "," + str(fpr)
        list_sensitivity.append(sensititivity)
        list_fpr.append(fpr)

    auc = 0.0 
    for j in range(0,999,1):
        dx =  -float(list_fpr[j+1]) + float(list_fpr[j]) 
        dy = (float(list_sensitivity[j+1]) + float(list_sensitivity[j]))/2
        area = dx * dy 
        auc += area
    # print("%.3f"% auc)

    return auc, list_sensitivity, list_fpr

list_pos_score = []
list_neg_score = []

positive_ds = "5fold_result"
f1 = open(positive_ds,"r")

for line in f1:
    line = line.strip()
    index = re.split("\s+",line)[0]
    index = int(index)
    score = re.split("\s+",line)[2]
    score = float(score)
    if index > 327:
        list_pos_score.append(score)
    else:
        list_neg_score.append(score)
f1.close()



list_unique_fpr = []
total_auc = 0.0
for i in range(1,6,1):
    begin_index = int (65.2 * i - 65.2)
    end_index = int (65.1 * i - 1) 

    sub_pos_list = []
    sub_neg_list = []

    for j in range(begin_index, end_index+1, 1):
        sub_neg_list.append( list_neg_score[j])
        sub_pos_list.append( list_pos_score[j])
    
    positive_count = len(sub_pos_list)
    negative_count = len(sub_neg_list)
    area, l_sen, l_fpr = auc(sub_pos_list, sub_neg_list, positive_count, negative_count)
    total_auc += area

    plt.plot(l_fpr, l_sen, color = "grey", linewidth = 0.5, alpha=0.5)

    # print(total_auc)

    # out_file_name = str(i) + ".csv"
    # out = open(out_file_name, "w")

    for e in l_fpr:
        if e not in list_unique_fpr:
            list_unique_fpr.append(e)
    
    list_unique_avg_sen = []
    for element in list_unique_fpr:
        total_sen = 0
        count = 0
        for m in range(0,len(l_fpr),1):
            if element == l_fpr[m]:
                count += 1
                total_sen += float(l_sen[m])
        
        if count != 0:
            avg_sen = total_sen / count
            outline = element + "," + str(avg_sen) + "\n"
            
            # out.write(outline)
    # out.close()

    # for element in list_unique_fpr:
    #     total_sen = 0
    #     count = 0
    #     for m in range(0,len(l_fpr),1):
    #         if element == l_fpr[m]:
    #             count += 1
    #             total_sen += float(l_sen[m])
        
    #     if count == 0:
    #         print(i,",",element)





# ## obtained the unique fpr value
# list_avg_sen = [] 
# list_upper = [] 
# list_lower = []
# for single_fpr in list_unique_fpr:
#     avg_sen_corresponding = 0.0 

#     for i in range(1,6,1):
#         count = 1
#         fold_sen_total = 0
#         begin_index = int (65.2* i - 65.2)
#         end_index = int (65.1 * i - 1) 
#         sub_pos_list = []
#         sub_neg_list = []
#         for j in range(begin_index, end_index+1, 1):
#             sub_neg_list.append( list_neg_score[j])
#             sub_pos_list.append( list_pos_score[j])
#         positive_count = len(sub_pos_list)
#         negative_count = len(sub_neg_list)
#         area, l_sen, l_fpr = auc(sub_pos_list, sub_neg_list, positive_count, negative_count)
#         length_list_fpr = len(l_fpr)

#         for j in range(0,length_list_fpr, 1):
#             if single_fpr == l_fpr[j]:
#                 count += 1
#                 fold_sen_total += float( l_sen[j])
#         fold_avg_sen = fold_sen_total / count

#         avg_sen_corresponding += fold_avg_sen / 5
    
#     list_avg_sen.append(avg_sen_corresponding)
    
#     upper = 0 
#     lower = 0
#     sd = 0
#     deviation_sq = 0 

#     for i in range(1,6,1):
#         count = 1
#         fold_sen_total = 0
#         begin_index = int (65.2* i - 65.2)
#         end_index = int (65.1 * i - 1) 
#         sub_pos_list = []
#         sub_neg_list = []
#         for j in range(begin_index, end_index+1, 1):
#             sub_neg_list.append( list_neg_score[j])
#             sub_pos_list.append( list_pos_score[j])
#         positive_count = len(sub_pos_list)
#         negative_count = len(sub_neg_list)
#         area, l_sen, l_fpr = auc(sub_pos_list, sub_neg_list, positive_count, negative_count)
#         length_list_fpr = len(l_fpr)

#         for j in range(0,length_list_fpr, 1):
#             if single_fpr == l_fpr[j]:
#                 count += 1
#                 fold_sen_total += float( l_sen[j])
#         fold_avg_sen = fold_sen_total / count

#         deviation_sq += math.pow (  (fold_avg_sen - avg_sen_corresponding),2)
        
#         sd += deviation_sq / 5

#     upper += avg_sen_corresponding + sd 
#     lower += avg_sen_corresponding - sd

#     list_upper.append(upper)
#     list_lower.append(lower)

# # print(len (list_upper))
# plt.plot(list_unique_fpr, list_avg_sen, color = "blue", linewidth = 3)
# plt.plot(list_unique_fpr, list_upper, color = "blue", alpha = 0.6)
# plt.plot(list_unique_fpr, list_lower,color = "blue", alpha = 0.6)

# length = len(list_unique_fpr)
# for i in range(0,length,1):
#     print(list_unique_fpr[i], ",",list_upper[i])





# plt.plot(list_unique_fpr,list_avg_sen)

#######################################################
########## SVM ROC ####################################

list_pos_score = []
positive_ds = "svm_pd.csv"
f1 = open(positive_ds,"r")

for line in f1:
    line = line.strip()
    score = re.split(",",line)[4]
    score = float(score)
    list_pos_score.append(score)
f1.close()

list_neg_score = []
negative_ds = "svm_nd.csv"
f2 = open(negative_ds,"r")
for line in f2:
    line = line.strip()
    score = re.split(",",line)[4]
    score = float(score)
    list_neg_score.append(score)
f2.close()

list_sensitivity = []
list_fpr = []

######  ROC curve
i = 0
while i <= 1:
    i += 0.001
    count_tp = 0 
    for p in list_pos_score:
        if p > i:
            count_tp += 1
    
    count_tn = 0
    for n in list_neg_score:
        if n < i:
            count_tn += 1
    
    sensititivity = count_tp / 326
    fpr = 1 - count_tn / 326

    sensititivity = "%.3f"% sensititivity
    fpr  = "%.3f"% fpr

    # out = str(i) + "," + str(sensititivity) + "," + str(fpr)

    list_sensitivity.append(sensititivity)
    list_fpr.append(fpr)


plt.plot(list_fpr, list_sensitivity)

plt.plot([0,1],[0,1], "--")

plt.title("ROC Curve", fontsize = 20)
plt.xlabel("False Positive Rate", fontsize = 16)
plt.ylabel("True Positive Rate", fontsize = 16)

plt.text(5,5,"TEST",fontsize = 12)

plt.show()
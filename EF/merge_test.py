#! /usr/bin/python3

import re 
import numpy as np 

#### for merge nd pd
#  for i in range(1,11,1):
#     file_path = "/home/bwang/project/hydrogenexchange/EF/vfold/fold" +  str(i) + "test.csv"
#     f = open(file_path,"r")
#     for line in f:
#         line = line.strip()
#         if line.startswith("datasetid,pdb,residue,") != 1:
#             exchange_info = re.split(",",line)[5]
#             if exchange_info != "non_ex":
#                 print(line)
#     f.close()

#### for randomly reorder shiffe
# random_index = np.random.choice(350,350,replace=False)
# list_line = []
# file = "rf_pd.csv"
# f = open(file,"r")
# for line in f:
#     line = line.strip()
#     list_line.append(line)
# f.close()

# for e in random_index:
#     print(list_line[e])

### for vold
list_pd = []
file = "random_pd.csv"
f = open(file,"r")
for line in f:
    line = line.strip()
    list_pd.append(line)
f.close()

list_nd = []
file = "random_nd.csv"
f = open(file,"r")
for line in f:
    line = line.strip()
    list_nd.append(line)
f.close()


for i in range(1,11,1):
    begin = 35 * i - 35 
    end = 35 * i - 1 

    list_test = []
    list_train = []

    for j in range(begin, end+1, 1):
        list_test.append(list_nd[j])
        
    for j in range(begin, end+1, 1):
        list_test.append(list_pd[j])

    
    for e in list_nd:
        if e not in list_test:
            list_train.append(e)
        
    for e in list_pd:
        if e not in list_test:
            list_train.append(e)
    
    out1 = "fold" + str(i) + "test.csv"
    out2 = "fold" + str(i) + "train.csv"

    o1 = open(out1,"w")
    o2 = open(out2,"w")

    for e in list_test:
        oline = e + "\n"
        o1.write(oline)
    
    for e in list_train:
        oline = e + "\n"
        o2.write(oline)
    
    o1.close()
    o2.close()

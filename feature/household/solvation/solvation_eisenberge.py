#! /usr/bin/python3

import re
import numpy
import math
import os

###########################################
#### Calculate Solvation Energy  ##########
#### Written by Boshen Wang ###############
#### Nature 319.6050 (1986) ###############
###########################################

# the dictionary residue => heavy atom including terminal OXT
dic_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "CB"),
    "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH2"),
    "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2"),
    "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2"),
    "CYS": ("N", "CA", "C", "O", "CB", "SG"),
    "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2"),
    "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2"),
    "GLY": ("N", "CA", "C", "O"),
    "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2"),
    "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1"),
    "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2"),
    "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ"),
    "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE"),
    "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ"),
    "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD"),
    "SER": ("N", "CA", "C", "O", "CB", "OG"),
    "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2"),
    "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2"),
    "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH"),
    "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2"),
}


def capture_heavy_atom(PDB_file_path):
    # list for Heavy atom entry
    list_heavy_atom_entry = []

    # list for residue alpha carbon
    list_residue_CA = []

    f = open(PDB_file_path, 'r')

    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip().upper()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip().upper()

            # chain identifier
            chain = line[21].upper()

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            residue_info = residue + "," +\
                residue_seq_number + "," +\
                chain

            heavy_atom_info = residue + "," +\
                residue_seq_number + "," +\
                chain + "," +\
                atom + "," +\
                x + "," + y + "," + z

            if atom in dic_residue_heavyatom[residue]:
                list_heavy_atom_entry.append(heavy_atom_info)
                if atom == "CA":
                    list_residue_CA.append(heavy_atom_info)

    f.close()

    return list_residue_CA, list_heavy_atom_entry


# reading the CASTP output file
def read_castp_output(castp_output_file):
    dic_atom_SASA = {}

    f = open(castp_output_file, "r")
    for line in f:
        if line.startswith("ATOM"):
            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # atom's sasa
            atom_sasa = ""
            for i in range(39, 46, 1):
                atom_sasa += line[i]
            atom_sasa = atom_sasa.strip()

            atom_label = residue + "," + residue_seq_number + "," + chain + "," + atom

            dic_atom_SASA[atom_label] = atom_sasa
    f.close()

    return dic_atom_SASA


# define the function to calculate desolvation energy within 10A
def solvation_energy_calculation(alpha_carbon_list, heavy_atom_list,
                                 dictionary_heavyatom_SASA):
    
    list_residue_solvation_energy = []

    # dictionary for Atom Reference SASA
    # C1 for non-aromatic carbon
    # C2 for aromatic carbon
    # C3 for other carbon (Carbonyl, etc)
    dic_ReferAtom_SASA = {
        "N": 106,
        "O": 99,
        "S": 133,
        "C1": 145,
        "C2": 133,
        "C3": 106,
    }

    # dicionary for solvation energy
    dic_energy = {
        "C": 16,
        "N": -6,  # netural N
        "O": -6,  # netural O
        "O-": -24,  # ASP/GLU
        "N+": -50,  # HIS/ARG/LYS
        "S": 21,
    }

    dic_carbon_type = {
        "C":  "C3",
        "CA": "C1",
        "CB": "C1",
        "CD": "C1",
        "CD1": ["C1", "C2"],  # C1 for ILE/LEU C2 for TRP/PHE/TYR
        "CD2": ["C1", "C2"],  # C1 for ILE/LEU C2 for TRP/PHE/TYR
        "CE":  "C1",
        "CE1": "C2",
        "CE2": "C2",
        "CE3": "C2",
        "CG":  "C1",
        "CG1": "C1",
        "CG2": "C1",
        "CZ":  "C2",
        "CZ2": "C2",
        "CZ3": "C2",
        "CH2": "C2",
    }

    for single_alpha_carbon in alpha_carbon_list:
        solvation_energy = 0.0

        CA_x = float(re.split(",", single_alpha_carbon)[-3])
        CA_y = float(re.split(",", single_alpha_carbon)[-2])
        CA_z = float(re.split(",", single_alpha_carbon)[-1])

        CA_residue_info = re.split(",", single_alpha_carbon)[0] + "," +\
            re.split(",", single_alpha_carbon)[1] + "," +\
            re.split(",", single_alpha_carbon)[2]

        for single_heavy_atom in heavy_atom_list:
            guest_x = float(re.split(",", single_heavy_atom)[-3])
            guest_y = float(re.split(",", single_heavy_atom)[-2])
            guest_z = float(re.split(",", single_heavy_atom)[-1])

            guest_residue_info = re.split(",", single_heavy_atom)[0] + "," +\
                re.split(",", single_heavy_atom)[1] + "," +\
                re.split(",", single_heavy_atom)[2]
            heavy_atom_residue = re.split(",", single_heavy_atom)[0]
            heavy_atom_type = re.split(",", single_heavy_atom)[3]

            guest_atom_info = guest_residue_info + "," + heavy_atom_type

            distance = math.sqrt(math.pow((CA_x - guest_x), 2) +
                                 math.pow((CA_y - guest_y), 2) +
                                 math.pow((CA_z - guest_z), 2))
            if CA_residue_info != guest_residue_info:
                if distance < 10:
                    folded_SASA = float(
                        dictionary_heavyatom_SASA[guest_atom_info])
                    reference_SASA = 0
                    if heavy_atom_type in ["N", "ND1", "ND2", "NE", "NE1", "NE2", "NH1", "NH2", "NZ"]:
                        reference_SASA += float(dic_ReferAtom_SASA["N"])
                    elif heavy_atom_type in ["O", "OD1", "OD2", "OG", "OG1", "OE1", "OE2", "OH"]:
                        reference_SASA += float(dic_ReferAtom_SASA["O"])
                    elif heavy_atom_type in ["SD", "SG"]:
                        reference_SASA += float(dic_ReferAtom_SASA["S"])
                    elif heavy_atom_type in ["C"]:
                        reference_SASA += float(dic_ReferAtom_SASA["C3"])
                    elif heavy_atom_type in ["CE1", "CE2", "CE3", "CZ", "CZ2", "CZ3", "CH2"]:
                        reference_SASA += float(dic_ReferAtom_SASA["C2"])
                    elif heavy_atom_type in ["CA", "CB", "CD", "CE", "CG", "CG1", "CG2"]:
                        reference_SASA += float(dic_ReferAtom_SASA["C1"])
                    elif heavy_atom_type in ["CD1", "CD2"]:
                        if heavy_atom_residue in ["ILE", "LEU"]:
                            reference_SASA += dic_ReferAtom_SASA["C1"]
                        else:
                            reference_SASA += dic_ReferAtom_SASA["C2"]

                    delta_SASA = reference_SASA - folded_SASA

                    atom_energy = 0
                    if heavy_atom_type in ["SD", "SG"]:
                        atom_energy += dic_energy["S"]
                    elif heavy_atom_type in ["C","CE1", "CE2", "CE3", "CZ", "CZ2", "CZ3", "CH2",
                                             "CA", "CB", "CD", "CE", "CG", "CG1", "CG2", "CD1", "CD2"]:
                        atom_energy += dic_energy["C"]
                    elif heavy_atom_type in ["OD2", "OE2"]:
                        atom_energy += dic_energy["O-"]
                    elif heavy_atom_type in  ["O", "OD1", "OG", "OG1", "OE1", "OH"]:
                        atom_energy += dic_energy["O"]
                    elif heavy_atom_type in ["NZ", "NH2", "ND1"]:
                        atom_energy += dic_energy["N+"]
                    elif heavy_atom_type in ["N", "ND2", "NE", "NE1", "NE2", "NH1"]:
                        atom_energy += dic_energy["N"]
                    
                    current_atom_solvation_contribution = atom_energy * delta_SASA
                    solvation_energy += current_atom_solvation_contribution
        # convert to kCal/mol unit
        solvation_energy = solvation_energy / 1000
        out_line = CA_residue_info + "," + str(solvation_energy)
        list_residue_solvation_energy.append(out_line)
    return list_residue_solvation_energy


# test_castp_output = "/data/Dropbox/project/HD/feature/sasa/castp_out/1a64.4.contrib"
# dictionary_test = read_castp_output(test_castp_output)
# test_pdb = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/1a64.pdb"
# l1, l2 = capture_heavy_atom(test_pdb)
# solvation_energy_calculation(l1, l2, dictionary_test)


out = open("solvation.csv","w")
# iterating the pdb folder
pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/"

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        pdb_file_path = subdir + file

        if file.endswith(".pdb"):
            pdb_id = re.split(".pdb", file)[0]

            castp_out_file_path = "/data/Dropbox/project/HD/feature/sasa/castp_out/" + pdb_id + ".4.contrib"

            try:
                dictionary_SASA = read_castp_output(castp_out_file_path)
                l1, l2 = capture_heavy_atom(pdb_file_path)

                list_solvation_energy = solvation_energy_calculation(l1, l2, dictionary_SASA)

                for element in list_solvation_energy:
                    out_line = pdb_id + "," + element + "\n"
                    out.write(out_line)

            except:
                print(pdb_id) 
out.close()
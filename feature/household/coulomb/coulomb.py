#! /usr/bin/python3

import re
import numpy
import math
import os

###########################################
#### Calculate Coulomb Interaction ########
#### At the Backbone Amide Hydrogen #######
#### Written by Boshen Wang ###############
###########################################


###########################################
#### Structure of the Script ##############
#### RemoveHydrogen => obtain heavy atom ##
#### AddHydrogen => Arrange H atom VSEPR ##


# the dictionary residue => heavy atom including terminal OXT
dic_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "CB", "OXT"),
    "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1", "OXT"),
    "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2", "OXT"),
    "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2", "OXT"),
    "CYS": ("N", "CA", "C", "O", "CB", "SG", "OXT"),
    "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2", "OXT"),
    "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2", "OXT"),
    "GLY": ("N", "CA", "C", "O", "OXT"),
    "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2", "OXT"),
    "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1", "OXT"),
    "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "OXT"),
    "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ", "OXT"),
    "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE", "OXT"),
    "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ", "OXT"),
    "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD", "OXT"),
    "SER": ("N", "CA", "C", "O", "CB", "OG", "OXT"),
    "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2"),
    "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2", "OXT"),
    "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH", "OXT"),
    "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "OXT"),
}


# INDEX list_heavy_atom
# [0] -- residue name
# [1] residue sequence number
# [2] chain ID
# [3] atom name
# [4][5][6] x y z

# INDEX list_residue
# [0] => residue tri letter
# [1] => sequence number
# [2] => chain ID

def capture_heavy_atom(PDB_file_path):
    # list for Heavy atom entry
    list_heavy_atom_entry = []

    # list for unique residue
    list_residue = []

    # list for charged center
    list_charged_center = []
    #list for charged residue
    list_charged_residue = []

    # list for backbone amide 
    list_backbone_amide = []

    f = open(PDB_file_path, 'r')

    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip().upper()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip().upper()

            # chain identifier
            chain = line[21].upper()

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            residue_info = residue + "," +\
                residue_seq_number + "," +\
                chain

            if residue_info not in list_residue:
                list_residue.append(residue_info)

            if residue in ["ASP", "GLU", "HIS", "CYS", "TYR", "LYS", "ARG"]:
                if residue_info not in list_charged_residue:
                    list_charged_residue.append(residue_info)

            heavy_atom_info = residue + "," +\
                residue_seq_number + "," +\
                chain + "," +\
                atom + "," +\
                x + "," + y + "," + z

            if atom in dic_residue_heavyatom[residue]:
                list_heavy_atom_entry.append(heavy_atom_info)
                if atom == "N":
                    list_backbone_amide.append(heavy_atom_info)
    f.close()

    # capture the charged residue, c&n terminal will be added in next
    for single_charged_residue in list_charged_residue:
        c_residue_triLetter = re.split(",", single_charged_residue)[0].strip()
        c_residue_seqnum = re.split(",", single_charged_residue)[1].strip()
        c_residue_chain = re.split(",", single_charged_residue)[2].strip()

        charge_center_x = 0
        charge_center_y = 0
        charge_center_z = 0

        for every_heavy_atom in list_heavy_atom_entry:
            hatom_residue_triLetter = re.split(
                ",", every_heavy_atom)[0].strip()
            hatom_residue_seqnum = re.split(",", every_heavy_atom)[1].strip()
            hatom_residue_chain = re.split(",", every_heavy_atom)[2].strip()
            hatom_atom_name = re.split(",", every_heavy_atom)[3].strip()
            hatom_x = re.split(",", every_heavy_atom)[4].strip()
            hatom_y = re.split(",", every_heavy_atom)[5].strip()
            hatom_z = re.split(",", every_heavy_atom)[6].strip()

            hatom_residue_info = hatom_residue_triLetter + "," + \
                hatom_residue_seqnum + "," + hatom_residue_chain
            if hatom_residue_info == single_charged_residue:

                if c_residue_triLetter == "ASP":
                    if hatom_atom_name in ["OD1", "OD2"]:
                        charge_center_x += float(hatom_x) / 2
                        charge_center_y += float(hatom_y) / 2
                        charge_center_z += float(hatom_z) / 2

                elif c_residue_triLetter == "GLU":
                    if hatom_atom_name in ["OE1", "OE2"]:
                        charge_center_x += float(hatom_x) / 2
                        charge_center_y += float(hatom_y) / 2
                        charge_center_z += float(hatom_z) / 2

                elif c_residue_triLetter == "HIS":
                    if hatom_atom_name in ["CG", "ND1", "CD2", "CE1", "NE2"]:
                        charge_center_x += float(hatom_x) / 5
                        charge_center_y += float(hatom_y) / 5
                        charge_center_z += float(hatom_z) / 5

                elif c_residue_triLetter == "CYS":
                    if hatom_atom_name == "SG":
                        charge_center_x += float(hatom_x)
                        charge_center_y += float(hatom_y)
                        charge_center_z += float(hatom_z)

                elif c_residue_triLetter == "TYR":
                    if hatom_atom_name == "OH":
                        charge_center_x += float(hatom_x)
                        charge_center_y += float(hatom_y)
                        charge_center_z += float(hatom_z)

                elif c_residue_triLetter == "LYS":
                    if hatom_atom_name == "NZ":
                        charge_center_x += float(hatom_x)
                        charge_center_y += float(hatom_y)
                        charge_center_z += float(hatom_z)

                elif c_residue_triLetter == "ARG":
                    if hatom_atom_name == "CZ":
                        charge_center_x += float(hatom_x)
                        charge_center_y += float(hatom_y)
                        charge_center_z += float(hatom_z)

        # keep 3 decimals for corresponding pdb format
        charge_residue_center_info = single_charged_residue + "," +\
            str("%0.3f" % charge_center_x) + "," +\
            str("%0.3f" % charge_center_y) + "," +\
            str("%0.3f" % charge_center_z)
        list_charged_center.append(charge_residue_center_info)

    # check the terminal residue and add to charged residue list
    list_seqnum = []
    for residue in list_residue:
        seqnum = re.split(",", residue)[1].strip()
        seqnum = int(seqnum)
        if seqnum not in list_seqnum:
            list_seqnum.append(seqnum)

    max_seqnum = max(list_seqnum)
    min_seqnum = min(list_seqnum)

    begin_residue_info = ""
    begin_x = 0
    begin_y = 0
    begin_z = 0

    end_residue_info = ""
    end_x = 0
    end_y = 0
    end_z = 0

    for heavy_atom in list_heavy_atom_entry:
        hatom_residue_triLetter = re.split(",", heavy_atom)[0].strip()
        hatom_residue_seqnum = re.split(",", heavy_atom)[1].strip()
        hatom_residue_chain = re.split(",", heavy_atom)[2].strip()
        hatom_atom_name = re.split(",", heavy_atom)[3].strip()

        hatom_x = re.split(",", heavy_atom)[4].strip()
        hatom_y = re.split(",", heavy_atom)[5].strip()
        hatom_z = re.split(",", heavy_atom)[6].strip()

        if int(hatom_residue_seqnum) == int(min_seqnum):
            if hatom_atom_name == "N":
                begin_residue_info = "NTR" + "," + hatom_residue_seqnum + "," + hatom_residue_chain
                begin_x += float(hatom_x)
                begin_y += float(hatom_y)
                begin_z += float(hatom_z)
                if residue_info not in list_charged_residue:
                    list_charged_residue.append(residue_info)

        elif int(hatom_residue_seqnum) == int(max_seqnum):
            if hatom_atom_name in ["O", "OXT"]:
                end_residue_info = "CTR" + "," + hatom_residue_seqnum + "," + hatom_residue_chain
                end_x += float(hatom_x) / 2
                end_y += float(hatom_y) / 2
                end_z += float(hatom_z) / 2

    begin_n_termi_info = begin_residue_info + "," + \
        str(begin_x) + "," + str(begin_y) + "," + str(begin_z)
    end_c_termi_info = end_residue_info + "," + \
        str("%0.3f" % end_x) + "," + str("%0.3f" %
                                         end_y) + "," + str("%0.3f" % end_z)

    list_charged_center.append(begin_n_termi_info)
    list_charged_center.append(end_c_termi_info)

    return list_residue, list_heavy_atom_entry, list_charged_center, list_backbone_amide


###########################################
#### VSEPR Theory to protonate H atom #####
###########################################
#### protonate the backbone amide, N-H anti-parallel C=O bond within the same peptide bond
#### ith N-H parallel to (i-1)th C=O
def Protonate_Backbone_Amide(heavy_atom_list, residue_list):
    list_amide_hydrogen = []

    for residue in residue_list:
        residue = residue.strip()
        residue_name = re.split(",", residue)[0]
        residue_seqnum = int (re.split(",", residue)[1])
        residue_chain = re.split(",", residue)[2]
        
        ## The C=O on the left peptide of current residue
        residue_C_carbonyl_x = 0
        residue_C_carbonyl_y = 0
        residue_C_carbonyl_z = 0

        residue_O_carbonyl_x = 0
        residue_O_carbonyl_y = 0
        residue_O_carbonyl_z = 0
        
        # the backbone amide of current residue 
        residue_amide_x = 0
        residue_amide_y = 0
        residue_amide_z = 0

        for heavy_atom in heavy_atom_list:
            heavy_atom_name = re.split(",", heavy_atom)[3]

            heavy_atom_x = float(re.split(",", heavy_atom)[4])
            heavy_atom_y = float(re.split(",", heavy_atom)[5])
            heavy_atom_z = float(re.split(",", heavy_atom)[6])

            heavy_atom_residue_name  =    re.split(",", heavy_atom)[0]
            heavy_atom_seqnum =   int( re.split(",", heavy_atom)[1])
            heavy_atom_chain =    re.split(",", heavy_atom)[2]

            if  residue_chain ==  heavy_atom_chain:
                if residue_seqnum == heavy_atom_seqnum + 1:
                    if heavy_atom_name == "C":
                        residue_C_carbonyl_x += heavy_atom_x
                        residue_C_carbonyl_y += heavy_atom_y
                        residue_C_carbonyl_z += heavy_atom_z
                    elif heavy_atom_name == "O":
                        residue_O_carbonyl_x += heavy_atom_x
                        residue_O_carbonyl_y += heavy_atom_y
                        residue_O_carbonyl_z += heavy_atom_z
                elif residue_seqnum == heavy_atom_seqnum:
                    if heavy_atom_name == "N":
                        residue_amide_x += heavy_atom_x
                        residue_amide_y += heavy_atom_y
                        residue_amide_z += heavy_atom_z

        # distance between C-O x,y,z
        CO_dx = residue_C_carbonyl_x - residue_O_carbonyl_x
        CO_dy = residue_C_carbonyl_y - residue_O_carbonyl_y
        CO_dz = residue_C_carbonyl_z - residue_O_carbonyl_z

        CO_distance = math.sqrt ( math.pow(CO_dx,2) + \
                                  math.pow(CO_dy,2) + \
                                  math.pow(CO_dz,2) )
        ## assign the H for amide Except the 1st residue
        if CO_distance != 0:
            NH_dx = CO_dx / CO_distance
            NH_dy = CO_dy / CO_distance
            NH_dz = CO_dz / CO_distance

            amide_H_x = "%.3f"% (residue_amide_x + NH_dx)
            amide_H_y = "%.3f"% (residue_amide_y + NH_dy)
            amide_H_z = "%.3f"% (residue_amide_z + NH_dz)

            new_H_amide_info = residue + "," +\
                               str(amide_H_x) + "," + str(amide_H_y) + "," +\
                               str(amide_H_z)

            list_amide_hydrogen.append(new_H_amide_info)

        ## assign the H for 1st residue
        # else:

    return list_amide_hydrogen


#########################################
#### Calculate Buried Ratio #############
#########################################
def calculate_buried_ratio(charged_list, heavy_atom_list):
    dic_residue_buried_ratio = {}

    for charged_center in charged_list:
        count_contact = 0
        buried_ratio = 0

        charged_center_info = re.split(",", charged_center)[0] + "," +\
            re.split(",", charged_center)[1] + "," +\
            re.split(",", charged_center)[2]
        host_x = float(re.split(",", charged_center)[3])
        host_y = float(re.split(",", charged_center)[4])
        host_z = float(re.split(",", charged_center)[5])

        for each_heavy_atom in heavy_atom_list:
            guest_x = float(re.split(",", each_heavy_atom)[4])
            guest_y = float(re.split(",", each_heavy_atom)[5])
            guest_z = float(re.split(",", each_heavy_atom)[6])

            distance = math.sqrt(math.pow((guest_x - host_x), 2) +
                                 math.pow((guest_y - host_y), 2) +
                                 math.pow((guest_z - host_z), 2))
            if distance < 15:
                count_contact += 1

        if count_contact < 280:
            buried_ratio += 0
        elif count_contact < 560:
            buried_ratio += (count_contact - 280) / 280
        else:
            buried_ratio += 1
        # keep 3 float digits  
        dic_residue_buried_ratio[charged_center_info] = "%0.3f" % buried_ratio
    return dic_residue_buried_ratio


###########################################
#### Coulomb Interaction Calculation  #####
###########################################
def calculate_coulomb ( charged_center_list, backbone_amide_hydrogen_list, 
              dictionary_BR_sidechain, dictionary_BR_proton):
    
    list_BB_H_CoulombShift = []
    
    # look amide hydrogen as H+ , two charged group bring 1 e
    for single_bb_amide_hydrogen in backbone_amide_hydrogen_list:

        coulomb_shift = 0.0 
        
        bb_amide_H_x = float  (re.split(",", single_bb_amide_hydrogen)[-3].strip())
        bb_amide_H_y = float  (re.split(",", single_bb_amide_hydrogen)[-2].strip())
        bb_amide_H_z = float  (re.split(",", single_bb_amide_hydrogen)[-1].strip())

        bb_amide_H_residue_info = re.split(",", single_bb_amide_hydrogen)[0] + "," +\
                                  re.split(",", single_bb_amide_hydrogen)[1] + "," +\
                                  re.split(",", single_bb_amide_hydrogen)[2]

        for single_charged_center in charged_center_list:
            charged_SC_x = float (re.split(",", single_charged_center)[-3].strip())
            charged_SC_y = float (re.split(",", single_charged_center)[-2].strip())
            charged_SC_z = float (re.split(",", single_charged_center)[-1].strip())

            charged_SC_residue_info = re.split(",", single_charged_center)[0] + "," +\
                                      re.split(",", single_charged_center)[1] + "," +\
                                      re.split(",", single_charged_center)[2]
            
            # H+ as the most strong acid, charged side chain as the base
            indicator_function = -1

            #distance bwtween amide proton and charged side chain
            dist  = math.sqrt ( math.pow( (charged_SC_x - bb_amide_H_x) ,2) +\
                                math.pow( (charged_SC_y - bb_amide_H_y) ,2) +\
                                math.pow( (charged_SC_z - bb_amide_H_z) ,2)  )
            
            # dielectric constant 
            avg_buried_ratio = ( float( dictionary_BR_sidechain[charged_SC_residue_info] )+\
                                 float( dictionary_BR_proton[bb_amide_H_residue_info] ) ) / 2
            
            epsilon = 160 - 130 * avg_buried_ratio
            
            #distance dependent ratio 
            omega = 0
            if dist < 4:
                omega += dist / 4
            elif 4 <= dist <= 10:
                omega += (10 - dist) / 6
            else:
                omega += 0 
            
            single_coulomb_shift = indicator_function * 244 * omega / (epsilon * dist)
            coulomb_shift += single_coulomb_shift
        
        current_shift = bb_amide_H_residue_info + "," + str(coulomb_shift)
        list_BB_H_CoulombShift.append(current_shift)
    return list_BB_H_CoulombShift


# test_pdb_file = "/home/bwang/project/hydrogenexchange/feature/test.pdb"
# l1,  l2, l3, l4 = capture_heavy_atom(test_pdb_file)

# list_amide_H = Protonate_Backbone_Amide(heavy_atom_list = l2, residue_list =l1 )

# dic_BR_SC = calculate_buried_ratio(charged_list = l3 , heavy_atom_list = l2)
# dic_BR_H = calculate_buried_ratio(charged_list = list_amide_H, heavy_atom_list = l2)

# dictionary_BB_H_Coulomb_shift = calculate_coulomb (charged_center_list = l3 , backbone_amide_hydrogen_list = list_amide_H , 
#                    dictionary_BR_sidechain = dic_BR_SC, dictionary_BR_proton = dic_BR_H)


out = open("coulombshift.csv","w")
# iterating the pdb folder
pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/"

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        file_path = subdir + file
        pdb_id = re.split(".pdb", file)[0]

        if file.endswith(".pdb"):
            try:
                list_residue, list_heavy_atom, list_charge_center, list_charge_residue  = capture_heavy_atom(file_path)
                list_amide_H = Protonate_Backbone_Amide(list_heavy_atom, list_residue)
                dic_BR_SC = calculate_buried_ratio( list_charge_center,  list_heavy_atom)
                dic_BR_H = calculate_buried_ratio(list_amide_H, list_heavy_atom)
                list_coulomb_shift = calculate_coulomb(list_charge_center, list_amide_H, dic_BR_SC, dic_BR_H)

                for element in list_coulomb_shift:
                    out_line = pdb_id + "," + element + "\n"
                    out.write(out_line)

            except:
                print(pdb_id) 
out.close()
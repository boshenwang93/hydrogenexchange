#! /usr/bin/python3

#####################################
#### Written by Boshen Wang #########
#### Work for Monomer ###############
#### Calculate C-Beta contact #######
#### C-beta <= 8A  ##################
#### GLY as exception as CA #########
#### classification by range  #######
##### Neighbor  <= 5 ################
##### Short  6-11 ###################
##### Medium 12-23 ##################
##### Long > 24 #####################

########## Citation #################
##### PMID: 23760879  ###############
##### PMID: 21928322  ###############
##### PMID: 24267585  ###############

import math
import re
import os




####### the dictionary residue_type[key] ==> heavy atoms[value]
###### residue type as string, heavy atoms as tuples
dic_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "CB"),
    "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1"),
    "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2"),
    "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2"),
    "CYS": ("N", "CA", "C", "O", "CB", "SG"),
    "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2"),
    "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2"),
    "GLY": ("N", "CA", "C", "O"),
    "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2"),
    "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1"),
    "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2"),
    "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ"),
    "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE"),
    "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ"),
    "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD"),
    "SER": ("N", "CA", "C", "O", "CB", "OG"),
    "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2"),
    "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2"),
    "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH"),
    "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2"),
}


#### dictionary Residue Tri_Letter => One Letter
dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V",
}


#### capture information from dssp output file
#### file name rule "PDB_ID.dssp"
#### secondary strcture, water contact number, etc

def capture_information_dssp(dssp_out_file):
    f = open(dssp_out_file, 'r')

    list_residue_ss_info = []

    for line in f:
        # delete the white space
        trim_line = line.strip()

        # capture the atom line
        if trim_line.endswith(".") == 0:
            if trim_line.startswith("#") == 0:

                seq_num = ''
                for i in range(5, 10, 1):
                    seq_num += line[i]
                seq_num = seq_num.strip()

                chain_id = line[11]

                residue_one = line[13]
                residue_type = ''

                if residue_one.islower():
                    residue_type += "CYS"
                else:
                    for k,v in dic_tri_single.items():
                        if v == residue_one:
                            residue_type += k

                ss_type = line[16]
                # three types of helix
                if ss_type == "H":
                    ss_type = "Alpha_Helix"
                elif ss_type == "G":
                    ss_type = "3_10_Helix"
                elif ss_type == "I":
                    ss_type = "Pi_helix"

                # two types of beta sheet
                elif ss_type == "E":
                    ss_type = "Beta_sheet"
                elif ss_type == "B":
                    ss_type = "Beta_bridge"

                ## HydrogenBond Turn
                elif ss_type == "T":
                    ss_type = "HydrogenBondedTurn"

                # otherwise as loop
                else:
                    ss_type = "Loop"

                current_residue_ss_info = residue_type + "," + chain_id + "," \
                    + seq_num + "," + ss_type 

                # print(current_residue_ss_info)
                list_residue_ss_info.append(current_residue_ss_info)
    f.close()

    DictionaryResidueDomainID = {}

    ## Assign the Domain 
    length_protein = len(list_residue_ss_info)

    #beginning index as 1
    domain_i = 1

    for i in range(0, length_protein, 1):

        if i != length_protein - 1:
            current_ss_type = re.split( ",", list_residue_ss_info[i])[3]
            next_ss_type = re.split( ",", list_residue_ss_info[i+1])[3]
            
            key = re.split( ",", list_residue_ss_info[i])[0] + "," +\
                  re.split( ",", list_residue_ss_info[i])[2] + "," +\
                  re.split( ",", list_residue_ss_info[i])[1]
                  
            value = re.split( ",", list_residue_ss_info[i])[3] + "," + str (domain_i)
            DictionaryResidueDomainID[key] = value

            if current_ss_type != next_ss_type:
                domain_i += 1

        else:
            key = re.split( ",", list_residue_ss_info[i])[0] + "," +\
                  re.split( ",", list_residue_ss_info[i])[2] + "," +\
                  re.split( ",", list_residue_ss_info[i])[1]
                  
            value = re.split( ",", list_residue_ss_info[i])[3] + "," + str (domain_i)
            DictionaryResidueDomainID[key] = value

    return DictionaryResidueDomainID
#### list_residue_ss_dID  INDEX
## 0 => Residue
## 1 => chain ID
## 2 => sequence number
## 3 => ss type 
## 4 => domain ID


def capture_c_beta(pdb_file_path):
    # list for C-beta atom entry
    list_cb_atom_entry = []

    f = open(pdb_file_path, 'r')
    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:
        line_record_identify = ""
        ## column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            if re.match("GLY", residue, flags=re.IGNORECASE):  ## Ignore case
                if re.match("CA", atom, flags=re.IGNORECASE):
                    temp_cb_atom_entry = residue + "," + residue_seq_number + "," + chain + "," + atom + "," + x + "," + y + "," + z
                    list_cb_atom_entry.append(temp_cb_atom_entry)
            else:
                if re.match("CB", atom, flags=re.IGNORECASE):
                    temp_cb_atom_entry = residue + "," + residue_seq_number + "," + chain + "," + atom + "," + x + "," + y + "," + z
                    list_cb_atom_entry.append(temp_cb_atom_entry)

    f.close()
    return list_cb_atom_entry


### INDEX list_heavy_atom
# [0] -- residue name
# [1] residue sequence number
# [2] chain ID
# [3] atom name
# [4][5][6] x y z


def calculate_contact(list_cb_atom, dictionary_residue_sstype_domainID):
    # Initialize the Output list which contain the contact less than 15A
    list_contact_8A = []

    for host_atom in list_cb_atom:
        host_residue_name = re.split(",", host_atom)[0]
        host_residue_seq_number = re.split(",", host_atom)[1]
        host_residue_chain = re.split(",", host_atom)[2]

        host_identifier = host_residue_name + "," + host_residue_seq_number + "," + host_residue_chain

        host_atom_name = re.split(",", host_atom)[3]
        host_x = re.split(",", host_atom)[4]
        host_y = re.split(",", host_atom)[5]
        host_z = re.split(",", host_atom)[6]

        host_x = float(host_x)
        host_y = float(host_y)
        host_z = float(host_z)

        host_domain_ID = dictionary_residue_sstype_domainID[host_identifier]

        host_ss_type_single = ""
        if re.findall ("helix", host_domain_ID, flags= re.IGNORECASE):
            host_ss_type_single += "H"
        elif re.findall ("beta", host_domain_ID, flags= re.IGNORECASE):
            host_ss_type_single += "S"
        elif re.findall ("turn", host_domain_ID, flags= re.IGNORECASE):
            host_ss_type_single += "T"
        elif re.findall ("loop", host_domain_ID, flags= re.IGNORECASE):
            host_ss_type_single += "L"

        IntraDomainContact = 0
        InterDomainContact = 0

        ## different pair type C(4,2) = 6 under inter domain contact
        ## H for helix  S for Beta sheet  T for HbondTurn L for loop
        HH = 0
        HS = 0
        HT = 0
        HL = 0
        SS = 0 
        ST = 0
        SL = 0
        TT = 0
        TL = 0
        LL = 0

        for guest_atom in list_cb_atom:
            guest_residue_name = re.split(",", guest_atom)[0]
            guest_residue_seq_number = re.split(",", guest_atom)[1]
            guest_residue_chain = re.split(",", guest_atom)[2]

            guest_identifier = guest_residue_name + "," + guest_residue_seq_number + "," + guest_residue_chain

            guest_atom_name = re.split(",", guest_atom)[3]
            guest_x = re.split(",", guest_atom)[4]
            guest_y = re.split(",", guest_atom)[5]
            guest_z = re.split(",", guest_atom)[6]

            guest_x = float(guest_x)
            guest_y = float(guest_y)
            guest_z = float(guest_z)

            guest_domain_ID = dictionary_residue_sstype_domainID[guest_identifier]

            guest_ss_type_single = ""
            if re.findall ("helix", guest_domain_ID, flags= re.IGNORECASE):
                guest_ss_type_single += "H"
            elif re.findall ("beta", guest_domain_ID, flags= re.IGNORECASE):
                guest_ss_type_single += "S"
            elif re.findall ("turn", guest_domain_ID, flags= re.IGNORECASE):
                guest_ss_type_single += "T"
            elif re.findall ("loop", guest_domain_ID, flags= re.IGNORECASE):
                guest_ss_type_single += "L"
            
            pair_ss_type = host_ss_type_single + guest_ss_type_single

            if guest_identifier != host_identifier:
                delta_seq_seperation = math.fabs(int(host_residue_seq_number) - int(guest_residue_seq_number))
                delta_seq_seperation = int(delta_seq_seperation)

                dx = host_x - guest_x
                dy = host_y - guest_y
                dz = host_z - guest_z

                distance_square = math.pow(dx, 2) + math.pow(dy, 2) + math.pow(dz, 2)
                distance = math.sqrt(distance_square)

                if distance < 8:
                    if guest_domain_ID == host_domain_ID:
                        IntraDomainContact += 1

                    elif guest_domain_ID != host_domain_ID:
                        InterDomainContact += 1
                        if pair_ss_type in ["HH"]:
                            HH += 1
                        elif pair_ss_type in ["HS", "SH"]:
                            HS += 1 
                        elif pair_ss_type in ["HT", "TH"]:
                            HT += 1 
                        elif pair_ss_type in ["HL", "LH"]:
                            HL += 1
                        elif pair_ss_type in ["SS"]:
                            SS += 1
                        elif pair_ss_type in ["ST", "TS"]:
                            ST += 1
                        elif pair_ss_type in ["SL", "LS"]:
                            SL += 1
                        elif pair_ss_type in ["TT"]:
                            TT += 1
                        elif pair_ss_type in ["TL", "LT"]:
                            TL += 1
                        elif pair_ss_type in ["LL"]:
                            LL += 1

        out_entry = host_identifier + "," +\
                    str(IntraDomainContact) + "," +\
                    str(InterDomainContact) + "," +\
                    str(HH) + "," + str(HS) + "," +\
                    str(HT) + "," + str(HL) + "," +\
                    str(SS) + "," + str(ST) + "," +\
                    str(SL) + "," + str(TT) + "," +\
                    str(TL) + "," + str(LL) 

        list_contact_8A.append(out_entry)

    return list_contact_8A



# pdb_file = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/1coe.pdb"
# dssp_file_path = "/data/Dropbox/project/HD/feature/ss/dssp_out/1coe.dssp"
# list_cb_atom = capture_c_beta(pdb_file)
# dic_residue_domainID = capture_information_dssp(dssp_file_path)
# list_contact = calculate_contact(list_cb_atom, dic_residue_domainID)


#################################################################
######### Iterate The Whole PDB file folder #####################
#################################################################

pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/"
output_file = "InterDomainContact.csv"

out = open(output_file,'w')

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        if file.endswith("pdb"):
            file_path = pdb_directory + file
            pdb_id = re.split('.pdb', file)[0]
            dssp_file_path = "/data/Dropbox/project/HD/feature/ss/dssp_out/" + pdb_id + ".dssp"

            try:
                list_cb_atom = capture_c_beta(file_path)
                dic_residue_domainID = capture_information_dssp(dssp_file_path)
                list_contact = calculate_contact(list_cb_atom, dic_residue_domainID)
                for element in list_contact:
                    out_entry = pdb_id + "," + element + "\n"
                    out.write(out_entry)
            except:
                print(pdb_id)

out.close()
#! /usr/bin/python3

####################################################
############ Written By Boshen Wang ################
##### Capture All Kinds of Heavy Atom Contact ######
########### Thershold set as 20A ###################
####################################################


import math
import re
import os

####### the dictionary residue_type[key] ==> heavy atoms[value]
###### residue type as string, heavy atoms as tuples

dic_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "CB"),
    "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1"),
    "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2"),
    "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2"),
    "CYS": ("N", "CA", "C", "O", "CB", "SG"),
    "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2"),
    "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2"),
    "GLY": ("N", "CA", "C", "O"),
    "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2"),
    "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1"),
    "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2"),
    "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ"),
    "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE"),
    "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ"),
    "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD"),
    "SER": ("N", "CA", "C", "O", "CB", "OG"),
    "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2"),
    "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2"),
    "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH"),
    "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2"),
}


def capture_heavy_atom_entry(pdb_file_path):

    # list for heavy atom entry 
    list_heavy_atom_entry = []

    f = open(pdb_file_path, 'r')

    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:
        line_record_identify = ""
        ## column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            # print(line_record_identify)
            # print(residue)
            # print(residue_seq_number)
            # print(chain)
            # print(atom)
            # print(x)
            # print(y)
            # print(z)

            if atom in dic_residue_heavyatom[residue]:
                temp_heavy_atom_entry = residue + "," + residue_seq_number + "," + chain + "," + atom + "," + x + "," + y + "," + z
                list_heavy_atom_entry.append(temp_heavy_atom_entry)
    f.close()
    return list_heavy_atom_entry


### INDEX list_heavy_atom
# [0] -- residue name
# [1] residue sequence number
# [2] chain ID
# [3] atom name
# [4][5][6] x y z



#######################################################
#### calculate the contact within 20A #################
#### Return 1. list of residues #######################
#### 2. contact information ###########################
#######################################################

def calculate_contact_profile(list_heavy_atom):
    # Initialize the Output list which contain the contact less than 20A
    list_contact_20A = []
    list_residues = []

    for host_atom in list_heavy_atom:
        host_residue_name = re.split(",", host_atom)[0]
        host_residue_seq_number = re.split(",", host_atom)[1]
        host_residue_chain = re.split(",", host_atom)[2]

        host_identifier = host_residue_name + "," + host_residue_seq_number + "," + host_residue_chain

        if host_identifier not in list_residues:
            list_residues.append(host_identifier)

        host_atom_name = re.split(",", host_atom)[3]
        host_x = re.split(",", host_atom)[4]
        host_y = re.split(",", host_atom)[5]
        host_z = re.split(",", host_atom)[6]

        host_x = float(host_x)
        host_y = float(host_y)
        host_z = float(host_z)

        for guest_atom in list_heavy_atom:
            guest_residue_name = re.split(",", guest_atom)[0]
            guest_residue_seq_number = re.split(",", guest_atom)[1]
            guest_residue_chain = re.split(",", guest_atom)[2]

            guest_identifier = guest_residue_name + "," + guest_residue_seq_number + "," + guest_residue_chain

            guest_atom_name = re.split(",", guest_atom)[3]
            guest_x = re.split(",", guest_atom)[4]
            guest_y = re.split(",", guest_atom)[5]
            guest_z = re.split(",", guest_atom)[6]

            guest_x = float(guest_x)
            guest_y = float(guest_y)
            guest_z = float(guest_z)

            if guest_identifier != host_identifier:
                dx = host_x - guest_x
                dy = host_y - guest_y
                dz = host_z - guest_z

                distance_square = math.pow(dx, 2) + math.pow(dy, 2) + math.pow(dz, 2)
                distance = math.sqrt(distance_square)

                if distance < 20:
                    output_entry = host_identifier + "," + host_atom_name + "," + guest_identifier + "," + guest_atom_name + "," + str(distance)
                    list_contact_20A.append(output_entry)

    return list_residues,list_contact_20A

######################################################
##### 3 layers #######################################
##### 0-5A Short_layer ###############################
##### 5-10A medium layer #############################
##### 10-15A long layer ##############################
######################################################

def obtain_contact_3layer(residue_list, contact_list_within_20A):
    list_contact_3layer = []
    for residue in residue_list:
        short_layer_contact = 0
        short_layer_strength = 0

        medium_layer_contact = 0
        medium_layer_strength = 0

        long_layer_contact = 0
        long_layer_strength = 0

        for contact_information in contact_list_within_20A:

            distance = re.split(',', contact_information)[-1]
            distance = float(distance)

            prefix_host_identifier = re.split(',', contact_information)[0] + "," + re.split(',', contact_information)[1] \
                                     + "," + re.split(',', contact_information)[2]

            if prefix_host_identifier == residue:
                if distance <= 5:
                    short_layer_contact += 1
                    short_layer_strength += math.pow(distance,-2)

                elif 5 < distance <= 10:
                    medium_layer_contact += 1
                    medium_layer_strength += math.pow(distance,-2)

                else:
                    long_layer_contact += 1
                    long_layer_strength += math.pow(distance,-2)
        total_contact = short_layer_contact + medium_layer_contact + long_layer_contact
        total_strength = short_layer_strength + medium_layer_strength + long_layer_strength

        output_entry = residue + "," + str(short_layer_contact) + "," + str(short_layer_strength) + "," + \
                       str(medium_layer_contact) + "," + str(medium_layer_strength) +"," + \
                       str(long_layer_contact) + "," + str(long_layer_strength) + "," + \
                       str(total_contact) + "," + str(total_strength)

        list_contact_3layer.append(output_entry)

    return list_contact_3layer


##########################################################
##### 3 layers may be rigid ##############################
##### set the resolution as 1A ###########################
##### from 0 to 20 A #####################################
##########################################################

def obtain_contact_information_as_1A_resolution(residue_list, contact_list_within_20A):
    list_contact_resolution_1A = []

    for residue in residue_list:
        contact_1A = 0
        contact_2A = 0
        contact_3A = 0
        contact_4A = 0
        contact_5A = 0
        contact_6A = 0
        contact_7A = 0
        contact_8A = 0
        contact_9A = 0
        contact_10A = 0
        contact_11A = 0
        contact_12A = 0
        contact_13A = 0
        contact_14A = 0
        contact_15A = 0
        contact_16A = 0
        contact_17A = 0
        contact_18A = 0
        contact_19A = 0
        contact_20A = 0

        for contact_information in contact_list_within_20A:
            host_residue = re.split(",", contact_information)[0] + "," + re.split(",", contact_information)[1] + "," + \
                           re.split(",", contact_information)[2]
            distance = re.split(",", contact_information)[-1]
            distance = float(distance)

            if host_residue == residue:
                if distance < 1.0:
                    contact_1A = contact_1A + 1
                elif distance < 2.0:
                    contact_2A = contact_2A + 1
                elif distance < 3.0:
                    contact_3A = contact_3A + 1
                elif distance < 4.0:
                    contact_4A = contact_4A + 1
                elif distance < 5.0:
                    contact_5A = contact_5A + 1
                elif distance < 6.0:
                    contact_6A = contact_6A + 1
                elif distance < 7.0:
                    contact_7A = contact_7A + 1
                elif distance < 8.0:
                    contact_8A = contact_8A + 1
                elif distance < 9.0:
                    contact_9A = contact_9A + 1
                elif distance < 10.0:
                    contact_10A = contact_10A + 1
                elif distance < 11.0:
                    contact_11A = contact_11A + 1
                elif distance < 12.0:
                    contact_12A = contact_12A + 1
                elif distance < 13.0:
                    contact_13A = contact_13A + 1
                elif distance < 14.0:
                    contact_14A = contact_14A + 1
                elif distance < 15.0:
                    contact_15A = contact_15A + 1
                elif distance < 16.0:
                    contact_16A = contact_16A + 1
                elif distance < 17.0:
                    contact_17A = contact_17A + 1
                elif distance < 18.0:
                    contact_18A = contact_18A + 1
                elif distance < 19.0:
                    contact_19A = contact_19A + 1
                else:
                    contact_20A = contact_20A + 1

        output_entry = residue + "," + \
                 str(contact_1A) + "," + \
                 str(contact_2A) + "," + \
                 str(contact_3A) + "," + \
                 str(contact_4A) + "," + \
                 str(contact_5A) + "," + \
                 str(contact_6A) + "," + \
                 str(contact_7A) + "," + \
                 str(contact_8A) + "," + \
                 str(contact_9A) + "," + \
                 str(contact_10A)+ "," + \
                 str(contact_11A)+ "," + \
                 str(contact_12A)+ "," + \
                 str(contact_13A)+ "," + \
                 str(contact_14A)+ "," + \
                 str(contact_15A)+ "," + \
                 str(contact_16A)+ "," + \
                 str(contact_17A) + "," + \
                 str(contact_18A) + "," + \
                 str(contact_19A) + "," + \
                 str(contact_20A)
        list_contact_resolution_1A.append(output_entry)

    return list_contact_resolution_1A


##########################################################
##### Iterate the whole folder ###########################
##########################################################

pdb_directory = "/data/Dropbox/project/HD/database/pdb_modified/"

output_file = "contact_resolution_1A_2.csv"
out = open(output_file,'w')

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        if file.endswith("pdb"):
            file_path = pdb_directory + file
            pdb_id = re.split('.pdb', file)[0]

            list_heavy_atom = capture_heavy_atom_entry(file_path)

            list_residues, list_contact_within_20A = calculate_contact_profile(list_heavy_atom)

            list_contact_resolution_1A = obtain_contact_information_as_1A_resolution(list_residues,list_contact_within_20A)

            for element in list_contact_resolution_1A:
                out_entry = pdb_id + "," + element + "\n"
                out.write(out_entry)

out.close()

###############################################################
#### Obtain 3 layers contact number and strength ##############
###############################################################

# pdb_directory = "/data/Dropbox/project/HD/database/pdb_modified/"
#
# output_file = "contact_3layers.csv"
# out = open(output_file,'w')
#
# for subdir, dirs, files in os.walk(pdb_directory):
#     for file in files:
#         if file.endswith("pdb"):
#             file_path = pdb_directory + file
#             pdb_id = re.split('.pdb', file)[0]
#
#             list_heavy_atom = capture_heavy_atom_entry(file_path)
#
#             list_residues, list_contact_within_20A = calculate_contact_profile(list_heavy_atom)
#
#             list_contact_3layers = obtain_contact_3layer(list_residues,list_contact_within_20A)
#
#             for element in list_contact_3layers:
#                 out_entry = pdb_id + "," + element + "\n"
#                 out.write(out_entry)
#
# out.close()
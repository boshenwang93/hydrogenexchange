#! /usr/bin/python3

#####################################
#### Written by Boshen Wang #########
#### Work for Monomer ###############
#### Calculate C-Beta contact #######
#### C-beta <= 8A  ##################
#### GLY as exception as CA #########
#### classification by range  #######
##### Neighbor  <= 5 ################
##### Short  6-11 ###################
##### Medium 12-23 ##################
##### Long > 24 #####################

########## Citation #################
##### PMID: 23760879  ###############
##### PMID: 21928322  ###############
##### PMID: 24267585  ###############

import math
import re
import os


####### the dictionary residue_type[key] ==> heavy atoms[value]
###### residue type as string, heavy atoms as tuples
dic_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "CB"),
    "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1"),
    "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2"),
    "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2"),
    "CYS": ("N", "CA", "C", "O", "CB", "SG"),
    "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2"),
    "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2"),
    "GLY": ("N", "CA", "C", "O"),
    "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2"),
    "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1"),
    "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2"),
    "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ"),
    "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE"),
    "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ"),
    "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD"),
    "SER": ("N", "CA", "C", "O", "CB", "OG"),
    "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2"),
    "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2"),
    "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH"),
    "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2"),
}


def capture_c_beta(pdb_file_path):
    # list for C-beta atom entry
    list_cb_atom_entry = []

    f = open(pdb_file_path, 'r')
    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:
        line_record_identify = ""
        ## column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            if re.match("GLY", residue, flags=re.IGNORECASE):  ## Ignore case
                if re.match("CA", atom, flags=re.IGNORECASE):
                    temp_cb_atom_entry = residue + "," + residue_seq_number + "," + chain + "," + atom + "," + x + "," + y + "," + z
                    list_cb_atom_entry.append(temp_cb_atom_entry)
            else:
                if re.match("CB", atom, flags=re.IGNORECASE):
                    temp_cb_atom_entry = residue + "," + residue_seq_number + "," + chain + "," + atom + "," + x + "," + y + "," + z
                    list_cb_atom_entry.append(temp_cb_atom_entry)

    f.close()
    return list_cb_atom_entry


### INDEX list_heavy_atom
# [0] -- residue name
# [1] residue sequence number
# [2] chain ID
# [3] atom name
# [4][5][6] x y z

def calculate_contact(list_cb_atom):
    # Initialize the Output list which contain the contact less than 15A
    list_contact_8A = []

    for host_atom in list_cb_atom:
        host_residue_name = re.split(",", host_atom)[0]
        host_residue_seq_number = re.split(",", host_atom)[1]
        host_residue_chain = re.split(",", host_atom)[2]

        host_identifier = host_residue_name + "," + host_residue_seq_number + "," + host_residue_chain

        host_atom_name = re.split(",", host_atom)[3]
        host_x = re.split(",", host_atom)[4]
        host_y = re.split(",", host_atom)[5]
        host_z = re.split(",", host_atom)[6]

        host_x = float(host_x)
        host_y = float(host_y)
        host_z = float(host_z)

        neighbor_contact = 0
        short_contact = 0
        medium_contact = 0
        long_contact = 0

        for guest_atom in list_cb_atom:
            guest_residue_name = re.split(",", guest_atom)[0]
            guest_residue_seq_number = re.split(",", guest_atom)[1]
            guest_residue_chain = re.split(",", guest_atom)[2]

            guest_identifier = guest_residue_name + "," + guest_residue_seq_number + "," + guest_residue_chain

            guest_atom_name = re.split(",", guest_atom)[3]
            guest_x = re.split(",", guest_atom)[4]
            guest_y = re.split(",", guest_atom)[5]
            guest_z = re.split(",", guest_atom)[6]

            guest_x = float(guest_x)
            guest_y = float(guest_y)
            guest_z = float(guest_z)

            if guest_identifier != host_identifier:
                delta_seq_seperation = math.fabs(int(host_residue_seq_number) - int(guest_residue_seq_number))
                delta_seq_seperation = int(delta_seq_seperation)

                dx = host_x - guest_x
                dy = host_y - guest_y
                dz = host_z - guest_z

                distance_square = math.pow(dx, 2) + math.pow(dy, 2) + math.pow(dz, 2)
                distance = math.sqrt(distance_square)

                if distance < 8:
                    if delta_seq_seperation <= 5:
                        neighbor_contact += 1
                    elif 6 <= delta_seq_seperation <= 11:
                        short_contact += 1
                    elif 12 <= delta_seq_seperation <= 23:
                        medium_contact += 1
                    elif 24 <= delta_seq_seperation:
                        long_contact += 1

        total = neighbor_contact + short_contact + medium_contact + long_contact
        out_entry = host_identifier + "," + str(total)
        list_contact_8A.append(out_entry)

    return list_contact_8A

#### list_contact_8A
# [0] => Host Residue Name
# [1] => Host Residue Seq Number
# [2] => Host Residue Chain
# [3] =>  Neighbor Contact Number
# [4] =>  Short-Range Contact Number
# [5] =>  Medium-Range Contact Number
# [6] => Long-Range Contact Number


#################################################################
######### Iterate The Whole PDB file folder #####################
#################################################################

pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/"
output_file = "contact_cb.csv"

out = open(output_file,'w')

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        if file.endswith("pdb"):
            file_path = pdb_directory + file
            pdb_id = re.split('.pdb', file)[0]

            list_cb_atom = capture_c_beta(file_path)
            list_contact_8A = calculate_contact(list_cb_atom)

            for element in list_contact_8A:
                out_entry = pdb_id + "," + element + "\n"
                out.write(out_entry)

out.close()

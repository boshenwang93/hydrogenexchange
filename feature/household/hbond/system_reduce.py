#! /usr/bin/python3

import os

# iterating the pdb folder
pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/"
H_add_directory = "/data/Dropbox/project/HD/feature/hbond/PDB_H/"

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        file_path = subdir + file
        H_add_file_path = H_add_directory + file

        command_line = "reduce.3.23.130521.linuxi386 -FLIP " + file_path + ">" + H_add_file_path
        os.system(command_line)

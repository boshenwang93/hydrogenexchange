#! /usr/bin/python3
import math
import re


##########################################
#### Neighboring Residue Effect ##########
#### Written by Boshen Wang ##############
####  PMID: 8234247 ######################
####  PMID: 8234246 ######################
##########################################

# dictionary for neighboring residue effect
#### left_acid, right_acid, left_base, right_base
# '0' for the undertermined value
dic_residue_shift = {
    "ALA": (0.00,  0.00,  0.00,  0.00),
    "ARG": (-0.59, -0.32,  0.08,  0.22),
    "ASN": (-0.58, -0.13,  0.49,  0.32),

    # _COO for pH>7 condition, _COOH for pH<7 condition
    "ASP_COO": (0.90,  0.58, -0.30, -0.18),
    "ASP_COOH": (-0.90, -0.12,  0.69,  0.60),
    "GLU_COO": (-0.90,  0.31, -0.51, -0.15),
    "GLU_COOH": (-0.60, -0.27,  0.24,  0.39),
    "CTR_COO": (0.96, 0, -1.80, 0),
    "CTR_COOH": (0.05, 0, 0, 0),

    "CYS": (-0.54, -0.46,  0.62,  0.55),

    "CYS2": (-0.74, -0.58,  0.55,  0.46),

    "GLY": (-0.22, 0.22,  0.27,  0.17),
    "GLN": (-0.47, -0.27,  0.06,  0.20),

    "HIS": (0, 0, -0.10,  0.14),
    "HIS+": (-0.80, -0.51,  0.80,  0.83),

    "ILE": (-0.91, -0.59, -0.73, -0.23),
    "LEU": (-0.57, -0.13, -0.58, -0.21),
    "LYS": (-0.56, -0.29, -0.04,  0.12),
    "MET": (-0.64, -0.28, -0.01,  0.11),
    "PHE": (-0.52, -0.43, -0.24,  0.06),

    "PRO_trans": (0, -0.19, 0, -0.24),
    "PRO_cis": (0, -0.85, 0, 0.60),

    "SER": (-0.44, -0.39,  0.37,  0.30),
    "THR": (-0.79, -0.47, -0.07,  0.20),
    "TRP": (-0.40, -0.44, -0.41, -0.11),
    "TYR": (-0.41, -0.37, -0.27,  0.05),
    "VAL": (-0.74, -0.30, -0.70, -0.14),

    "NTR": (0, -1.32, 0, 1.62),

    "EMPTY": (0, 0, 0 , 0),
}

# K_ala under 293 K, acid, basic, water
# NAcMA for N-Ac-Ala-N'MA
# PDLA for poly-DL-alanine
# as minute inverse unit
K_ala_A_NAcMA = math.pow(10, 2.04) 
K_ala_B_NAcMA = math.pow(10, 10.36) 
K_ala_W_NAcMA = math.pow(10, -1.50) 

# in Englander's spreadsheet, they use PDLA as reference
K_ala_A_PDLA = math.pow(10, 1.62) 
K_ala_B_PDLA = math.pow(10, 10.05) 
K_ala_W_PDLA = math.pow(10, -1.50) 

# K for D2O solution constant [D-][OD-] under 20 C degree
K_D2O = 8.9E-16

# the pKD shift under unfolded state
# ATTENTION: Acidic residue (ASP, GLU, CTR) needs specific clarification
# accoding to the pH


def loop_shift(left_residue, right_residue, D_concentration, Temperature):
    T = Temperature

    # concentration of D+ & OD-
    D = D_concentration
    OD = K_D2O / D

    ala_ex_ref = math.pow(10, 1.19) * D +\
                 math.pow(10, 9.90) * OD +\
                 math.pow(10, -2.50)

    # shift constant from dictionary
    shift_A_L = dic_residue_shift[left_residue][0]
    shift_A_R = dic_residue_shift[right_residue][1]
    shift_B_L = dic_residue_shift[left_residue][2]
    shift_B_R = dic_residue_shift[right_residue][3]

    A_L = math.pow(10, (shift_A_L + math.log(ala_ex_ref,10)) )
    A_R = math.pow(10, (shift_A_R + math.log(ala_ex_ref,10)) )
    B_L = math.pow(10, (shift_B_L + math.log(ala_ex_ref,10)) )
    B_R = math.pow(10, (shift_B_R + math.log(ala_ex_ref,10)) )

    # under 293 K temperature
    K_loop_D  =  K_ala_A_PDLA * (A_L * A_R) * D 
    K_loop_OD =  K_ala_B_PDLA * (B_L * B_R) * OD 
    K_loop_w  =  K_ala_W_PDLA * (B_L * B_R)
    K_loop = K_loop_D + K_loop_OD + K_loop_w
    
    print(T,D, OD, ala_ex_ref, A_L, A_R, B_L, B_R, K_loop_D ,K_loop_OD , K_loop_w,K_loop)

    # with temperature adjustment
    R = 1.987  # gas constant
    Ea = 14000  # Activation Energy

    K_loop = K_loop * math.pow(math.e, (- Ea * (1 / T - 1 / 293) / R))

    return K_loop



Kloop = loop_shift("ALA","TRP",D_concentration= math.pow(10, -7.0),Temperature=279)
print(Kloop)

# read the pdb into the sequence mode
# return the list_residue
def read_pdb_residue(pdb_file_path):
    # initilize list storing residues
    list_residue = []

    f = open(pdb_file_path, 'r')
    for line in f:
        if line.startswith("ATOM"):
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # signle assitant identity charater
            sig_atom = line[76] + line[77]
            sig_atom = sig_atom.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            temp_atom_entry = residue + "," + residue_seq_number + "," + chain

            if temp_atom_entry not in list_residue:
                list_residue.append(temp_atom_entry)
    f.close()

    return list_residue


# clarify the experiment condition
def calculate_shift(pdb_file_path, D_concentration, Temperature, pH):

    list_residue = read_pdb_residue(pdb_file_path)

    for host_residue in list_residue:
        host_chain = re.split(",", host_residue)[2]
        host_seqnum = re.split(",", host_residue)[1]
        host_seqnum = int(host_seqnum)

        left_residue = ""
        right_residue = ""

        for guest_residue in list_residue:
            guest_residue_tri = re.split(",", guest_residue)[0]
            guest_seqnum = re.split(",", guest_residue)[1]
            guest_chain = re.split(",", guest_residue)[2]
            guest_seqnum = int(guest_seqnum)

            if guest_chain == host_chain:
                if guest_seqnum == host_seqnum - 1:
                    left_residue += guest_residue_tri
                elif guest_seqnum == host_seqnum + 1:
                    right_residue += guest_residue_tri
        
        if left_residue == "":
            left_residue += "EMPTY"
        
        if right_residue == "":
            right_residue += "EMPTY"
        # check the left/right residue
        # out_line = left_residue + "," + host_residue + "," +  right_residue
        # print(out_line)

        ## for pH condition, adjust polar residue's name
        if pH > 7 :
            if left_residue in ["ASP", "GLU", "CTR"]:
                left_residue = left_residue + "_COO"
            if right_residue in ["ASP", "GLU", "CTR"]:
                right_residue = right_residue + "_COO"
        elif pH <= 7 :
            if left_residue in ["ASP", "GLU", "CTR"]:
                left_residue = left_residue + "_COOH"
            if right_residue in ["ASP", "GLU", "CTR"]:
                right_residue = right_residue + "_COOH"

        host_residue_PF_shift = loop_shift(
            left_residue, right_residue, D_concentration, Temperature)
        
        out = host_residue + "," + str(host_residue_PF_shift)
        print(out)



# test_pdb_file = "test.pdb"

# calculate_shift(test_pdb_file, 1, 293, 7)

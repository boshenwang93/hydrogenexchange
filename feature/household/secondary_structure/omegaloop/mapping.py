#! /usr/bin/python3

import re

out_file = "ss_info.csv"
out  = open(out_file,'w')

dssp_file = "/home/bwang/project/hydrogenexchange/csv/dssp_info.csv"

dssp = open(dssp_file,"r")
for line in dssp:
    ss = re.split(",",line)[4]
    if ss != "Loop":
        out.write(line)
dssp.close()

loop_file = "/home/bwang/project/hydrogenexchange/csv/classified_Loop.csv"

loop = open(loop_file,"r")
for line in loop:
    out.write(line)
loop.close()

out.close()

list_res = []

r_out = open(out_file,"r")
for line in r_out:
    residue_info = ""
    for i in range(0,4,1):
        residue_info += re.split(",",line)[i]
    
    if residue_info not in list_res:
        list_res.append(residue_info)
r_out.close()

print(len(list_res))
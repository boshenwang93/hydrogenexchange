#! /usr/bin/python3
import numpy
import math
import re
import os

##################################################################
#### Written By Boshen Wang ######################################
#### For purpose of classify loops defined by Ring & Cohen #######
#### Ring, Christine S., et al. ##################################
#### "Taxonomy and conformational analysis of loops in proteins."#
#### Journal of molecular biology 224.3 (1992): 685-699. #########
##################################################################

##################################################################
#### 1. inertia_tensor_transform retrurn the linearity, flatness #
#### 2. capture_calpha_entry return C-alpha atom #################
#### 3. classify_loop return the strap/omega/zeta loop ###########
#### 4. overlap solve the overlapping issue ######################
##################################################################


# Given the Average Absolute distance of the loop alpha carbon
#### E_x, E_y, E_z
# Input xyz as list of list
# Like [[0,0,1],[1,2,3],[1,2,1],[],......]
# Return the linearity(E_y/E_x) & Flatness
def inertia_tensor_transform(origin_xyz_list_list):

    # capture the length of residues
    # the length of particicle
    length_seg = len(origin_xyz_list_list)

    # seek the center of mass
    G_x = 0.0
    G_y = 0.0
    G_z = 0.0

    motion_xyz = []

    for i in range(0, length_seg, 1):
        x_co = float(origin_xyz_list_list[i][0])
        y_co = float(origin_xyz_list_list[i][1])
        z_co = float(origin_xyz_list_list[i][2])

        G_x += x_co / length_seg
        G_y += y_co / length_seg
        G_z += z_co / length_seg

    for i in range(0, length_seg, 1):
        x_co = float(origin_xyz_list_list[i][0])
        y_co = float(origin_xyz_list_list[i][1])
        z_co = float(origin_xyz_list_list[i][2])

        motion_x = x_co - G_x
        motion_y = y_co - G_y
        motion_z = z_co - G_z

        new_atom_xyz = [motion_x, motion_y, motion_z]
        motion_xyz.append(new_atom_xyz)

    I_xx = 0
    I_xy = 0
    I_xz = 0

    I_yx = 0
    I_yy = 0
    I_yz = 0

    I_zx = 0
    I_zy = 0
    I_zz = 0

    # set the mass of the carbon as 1
    m = 1

    # calculate the inertia tensor according to the gravity point
    for i in range(0, length_seg, 1):
        x_co = float(motion_xyz[i][0])
        y_co = float(motion_xyz[i][1])
        z_co = float(motion_xyz[i][2])

        I_xx += m * (math.pow(y_co, 2) + math.pow(z_co, 2))
        I_yy += m * (math.pow(x_co, 2) + math.pow(z_co, 2))
        I_zz += m * (math.pow(x_co, 2) + math.pow(y_co, 2))

        I_xy += m * x_co * y_co
        I_xz += m * x_co * z_co

        I_yx += m * y_co * x_co
        I_yz += m * y_co * z_co

        I_zx += m * z_co * x_co
        I_zy += m * z_co * y_co

    I_matrix = numpy.matrix([[I_xx, -I_xy, -I_xz],
                             [-I_yx, I_yy, -I_yz],
                             [-I_zx, -I_zy, I_zz]])

    eigen_value, eigen_vector = numpy.linalg.eigh(I_matrix)

    E_x = 0.0
    E_y = 0.0
    E_z = 0.0

    for i in range(0, length_seg, 1):
        x_co = float(motion_xyz[i][0])
        y_co = float(motion_xyz[i][1])
        z_co = float(motion_xyz[i][2])

        coordinate_vector = numpy.array([x_co, y_co, z_co])

        x_dot = numpy.dot(coordinate_vector.reshape(1, 3),
                          eigen_vector[0].reshape(3, 1))
        y_dot = numpy.dot(coordinate_vector.reshape(1, 3),
                          eigen_vector[1].reshape(3, 1))
        z_dot = numpy.dot(coordinate_vector.reshape(1, 3),
                          eigen_vector[2].reshape(3, 1))

        E_x += abs(x_dot) / length_seg
        E_y += abs(y_dot) / length_seg
        E_z += abs(z_dot) / length_seg

    # sort the E_x E_y E_z
    max_extent = max(E_x, E_y, E_z)
    min_extent = min(E_x, E_y, E_z)
    mid_extent = E_x + E_y + E_z - max_extent - min_extent

    if max_extent != 0:
        linearity = mid_extent / max_extent
        flatness = math.sqrt(2) * min_extent / \
            math.sqrt(math.pow(max_extent, 2) + math.pow(mid_extent, 2))
    else:
        linearity = "Not Exist" + str(E_x) + "," + str(E_y) + "," + str(E_z)
        flatness = "Not Exist"

    return linearity, flatness

# define the function captures c-alpha atoms from pdb file
# works for monomer


def capture_calpha_entry(pdb_file_path):
    # initialize list for c-alpha atom
    list_c_alpha = []

    f = open(pdb_file_path, 'r')
    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            temp_atom_entry = atom + "," + residue + "," +\
                residue_seq_number + "," + chain + "," + x + "," + y + "," + z

            if atom == "CA":
                list_c_alpha.append(temp_atom_entry)

    f.close()

    return list_c_alpha


# function return the continuous segements (4-20) from c-alpha list
def segment_gen(CA_list):
    list_list_segments = []

    for begin_ca in CA_list:
        begin_seq_number = re.split(",", begin_ca)[2]
        begin_seq_number = int(begin_seq_number)

        for k in range(4, 21, 1):
            end_seq_number = begin_seq_number + k

            current_con_seg = []
            current_con_seg.append(begin_ca)

            for other_ca in CA_list:
                other_ca_seq_number = re.split(",", other_ca)[2]
                other_ca_seq_number = int(other_ca_seq_number)

                if begin_seq_number < other_ca_seq_number <= end_seq_number:
                    current_con_seg.append(other_ca)

            if len(current_con_seg) == k:
                if current_con_seg not in list_list_segments:
                    list_list_segments.append(current_con_seg)

    return list_list_segments

# calculate inertia tensor for each segment
# return the clasificition of loop


def classification_loop(list_list_segments):
    list_zeta = []
    list_omega = []
    list_strap = []

    for single_segment in list_list_segments:
        list_list_xyz = []
        list_seqnum = []
        list_residuen_identify = []

        for each_ca in single_segment:
            seq_num = int(re.split(",", each_ca)[2])
            res_tri = re.split(",", each_ca)[1]
            chain = re.split(",", each_ca)[3]
            residue_info = res_tri + "," + str(seq_num) + "," + chain

            list_seqnum.append(seq_num)
            list_residuen_identify.append(residue_info)

            x_co = re.split(",", each_ca)[4]
            y_co = re.split(",", each_ca)[5]
            z_co = re.split(",", each_ca)[6]
            list_xyz = [x_co, y_co, z_co]

            list_list_xyz.append(list_xyz)

        linearity, flatness = inertia_tensor_transform(list_list_xyz)
        linearity = float(linearity)
        flatness = float(flatness)

        initial_seqnum = min(list_seqnum)
        terminial_seqnum = max(list_seqnum)

        if linearity > 0.5:
            if flatness > 0.4:
                list_zeta.append(list_residuen_identify)
                # print(initial_seqnum,terminial_seqnum,linearity,flatness)
            else:
                list_omega.append(list_residuen_identify)
        else:
            list_strap.append(list_residuen_identify)

    return list_zeta, list_omega, list_strap

# solve overlapping issue
# set the prority as zeta > omega > linear

list_zeta_residue = []
list_omega_residue = []
list_strap_residue = []

# iterate the Loop PDB
loop_pdb_directory = "/data/Dropbox/project/HD/feature/ss/loop_pdb/"

for subdir, dirs, files in os.walk(loop_pdb_directory):

    for file in files:
        loop_path = subdir + file

        pdbid = ''
        for i in range(0, 4, 1):
            pdbid += file[i]

        ca_list = capture_calpha_entry(loop_path)
        seg_list = segment_gen(ca_list)
        list_zeta_seg, list_omega_seg, list_strap_seg = classification_loop(seg_list)

        for zeta_seg in list_zeta_seg:
            for zeta_residue in zeta_seg:
                if zeta_residue not in list_zeta_residue:
                    out = pdbid + "," + zeta_residue
                    list_zeta_residue.append(out)
        
        for omega_seg in list_omega_seg:
            for omega_residue in omega_seg:
                if omega_residue not in list_omega_residue:
                    out = pdbid + "," + omega_residue
                    list_omega_residue.append(out)

        for strap_seg in list_strap_seg:
            for strap_residue in strap_seg:
                if strap_residue not in list_strap_residue:
                    out = pdbid + "," + strap_residue
                    list_strap_residue.append(out)

need_mapped_loop_file = "/home/bwang/project/hydrogenexchange/csv/all_loop_residue.csv"

loop = open(need_mapped_loop_file,"r")

for line in loop:
    residue = re.split("\n",line)[0]
    
    if residue in list_zeta_residue:
        if residue in list_omega_residue:
            out = residue + "," + "Compound"
            print(out)
        else:
            out = residue + "," + "Zeta"
            print(out)
    elif residue in list_omega_residue:
        out = residue + "," + "Omega"
        print(out)
    elif residue in list_strap_residue:
        out = residue + "," + "Strap"
        print(out)
    else:
        out  = residue + "," + "Unclassified"
        print(out)

loop.close()

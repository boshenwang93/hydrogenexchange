#! /usr/bin/python3
import re
import math

###########################################################
### capture omega loops in protein 3-d structure ##########
### Written By B.WANG #####################################
### CitationLeszczynski, Jacquelyn F., and George D. Rose.#
###  "Loops in globular proteins: a novel category of secondary structure." 
###  Science 234.4778 (1986): 849-855. ###################
##########################################################


##########################################################
######## Definition of Omega Loops #######################
######## 1. 6-16 resisdues  ############################## 
######## 2. Distance of termini less than 10 A ###########
######## 3. Distance of termini less than 2/3 Max C-a ####
##########################################################

##### define the function captures c-alpha atoms from pdb file 
##### works for monomer 
def capture_calpha_entry(pdb_file_path):
    # initialize list for c-alpha atom 
    list_c_alpha = []


    f = open(pdb_file_path,'r')
    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:
        line_record_identify = ""
        ## column 1-6 is the identfication for the line record
        for i in range(0,6,1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12,16,1):
                atom += line[i]
            atom = atom.strip()

            #atom x, y, z
            x = ""
            for i in range(30,38,1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38,46,1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46,54,1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17,20,1):
                residue += line[i]
            residue = residue.strip()

            #chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22,26,1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()


            temp_atom_entry = atom + "," + residue + "," +\
             residue_seq_number + "," + chain + "," + x + "," + y + "," + z

            if atom == "CA":
                list_c_alpha.append(temp_atom_entry)
            
    f.close()

    return list_c_alpha


###define the function return the continuous segements (6-16) from c-alpha list
### return the redundant omega loops list 
def calculate_omega_loop (CA_list):
    list_omega_loop = []

    for begin_ca in CA_list:
        begin_seq_number = re.split(",",begin_ca)[2]
        begin_seq_number = int(begin_seq_number)

        for k in range(5,16,1):
            end_seq_number = begin_seq_number + k

            current_con_seg = []
            current_con_seg.append(begin_ca)

            for other_ca in CA_list:
                other_ca_seq_number = re.split(",",other_ca)[2]
                other_ca_seq_number = int(other_ca_seq_number)

                if begin_seq_number < other_ca_seq_number <= end_seq_number:
                    current_con_seg.append(other_ca)
            
            ## must contain >= 6 residues 
            if len(current_con_seg) >= 6:

                ##obtain maximum c-a distance
                ##obtain distance of termini
                max_ca_dist = 0.0
                dist_termini = 0.0

                for entry in current_con_seg:
                    host_x = re.split(",",entry)[4]
                    host_x = float(host_x)
                    host_y = re.split(",",entry)[5]
                    host_y = float(host_y)
                    host_z = re.split(",",entry)[6]
                    host_z = float(host_z)

                    host_seq_number =re.split(",",entry)[2]
                    host_seq_number = int(host_seq_number)

                    for another_entry in current_con_seg:
                        if another_entry != entry:
                            guest_x = re.split(",",another_entry)[4]
                            guest_x = float(guest_x)
                            guest_y = re.split(",",another_entry)[5]
                            guest_y = float(guest_y)
                            guest_z = re.split(",",another_entry)[6]
                            guest_z = float(guest_z)

                            delta_x = guest_x - host_x
                            delta_y = guest_y - host_y
                            delta_z = guest_z - host_z

                            distance_square = math.pow(delta_x,2) + math.pow(delta_y,2) + math.pow(delta_z,2)
                            distance = math.sqrt(distance_square)
                            
                            if distance > max_ca_dist:
                                max_ca_dist = distance
                            
                            guest_seq_number = re.split(",",another_entry)[2]
                            guest_seq_number = int(guest_seq_number)

                            delta_seq = abs(host_seq_number - guest_seq_number)
                            if (delta_seq + 1) == len(current_con_seg):
                                dist_termini = distance

                if dist_termini <= 10:
                    if max_ca_dist >= (1.5 * dist_termini):
                        list_omega_loop.append(current_con_seg)

    return list_omega_loop







###solve Overlapping issue
##
# def compact_omega_loop(omega_loop_list):
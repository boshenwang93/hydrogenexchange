#! /usr/bin/python3
import numpy
import math
import matplotlib

# Given the Average Absolute distance of the loop alpha carbon
# E_x, E_y, E_z
# Input xyz as list of list
# Like [[0,0,1],[1,2,3],[1,2,1],[],......]
# Return the linearity(E_y/E_x) & Flatness

def inertia_tensor_transform(origin_xyz_list_list):

    # capture the length of residues
    # the length of particicle
    length_seg = len(origin_xyz_list_list)

    # seek the center of mass
    G_x = 0.0
    G_y = 0.0
    G_z = 0.0

    motion_xyz = []

    for i in range(0, length_seg, 1):
        x_co = float(origin_xyz_list_list[i][0])
        y_co = float(origin_xyz_list_list[i][1])
        z_co = float(origin_xyz_list_list[i][2])

        G_x += x_co / length_seg
        G_y += y_co / length_seg
        G_z += z_co / length_seg

    for i in range(0, length_seg, 1):
        x_co = float(origin_xyz_list_list[i][0])
        y_co = float(origin_xyz_list_list[i][1])
        z_co = float(origin_xyz_list_list[i][2])

        motion_x = x_co - G_x
        motion_y = y_co - G_y
        motion_z = z_co - G_z

        new_atom_xyz = [motion_x, motion_y, motion_z]
        motion_xyz.append(new_atom_xyz)

    I_xx = 0
    I_xy = 0
    I_xz = 0

    I_yx = 0
    I_yy = 0
    I_yz = 0

    I_zx = 0
    I_zy = 0
    I_zz = 0

    # set the mass of the carbon as 1
    m = 1

    # calculate the inertia tensor according to the gravity point
    for i in range(0, length_seg, 1):
        x_co = float(motion_xyz[i][0])
        y_co = float(motion_xyz[i][1])
        z_co = float(motion_xyz[i][2])

        I_xx += m * (math.pow(y_co, 2) + math.pow(z_co, 2))
        I_yy += m * (math.pow(x_co, 2) + math.pow(z_co, 2))
        I_zz += m * (math.pow(x_co, 2) + math.pow(y_co, 2))

        I_xy += m * x_co * y_co
        I_xz += m * x_co * z_co

        I_yx += m * y_co * x_co
        I_yz += m * y_co * z_co

        I_zx += m * z_co * x_co
        I_zy += m * z_co * y_co

    I_matrix = numpy.matrix([[I_xx, -I_xy, -I_xz], 
                             [-I_yx, I_yy, -I_yz],
                             [-I_zx, -I_zy, I_zz]])

    eigen_value, eigen_vector = numpy.linalg.eigh(I_matrix)
    
    E_x = 0.0
    E_y = 0.0
    E_z = 0.0

    for i in range(0, length_seg, 1):
        x_co = float(motion_xyz[i][0])
        y_co = float(motion_xyz[i][1])
        z_co = float(motion_xyz[i][2])

        coordinate_vector = numpy.array([x_co, y_co, z_co])

        x_dot = numpy.dot(coordinate_vector.reshape(1,3), eigen_vector[0].reshape(3,1))
        y_dot = numpy.dot(coordinate_vector.reshape(1,3), eigen_vector[1].reshape(3,1))
        z_dot = numpy.dot(coordinate_vector.reshape(1,3), eigen_vector[2].reshape(3,1))


        E_x += abs(x_dot) / length_seg
        E_y += abs(y_dot) / length_seg
        E_z += abs(z_dot) / length_seg
    
    #sort the E_x E_y E_z 
    max_extent = max(E_x,E_y,E_z)
    min_extent = min(E_x,E_y,E_z)
    mid_extent = E_x + E_y + E_z - max_extent - min_extent

    if max_extent != 0:
        linearity = mid_extent / max_extent
        flatness = math.sqrt(2) * min_extent / \
                   math.sqrt(math.pow(max_extent, 2) + math.pow(mid_extent, 2))
    else:
        linearity = "Not Exist" + str(E_x) + "," + str(E_y) + "," + str(E_z)
        flatness = "Not Exist"
    
    print(eigen_value,eigen_vector)
    return linearity, flatness


# line
test1 = [[0, 1, 0], [1, 1, 0], [3, 1, 0], [10, 1, 0]]
test4 = [[0, 0, 1], [0, 1, 1], [0, -10, 1]]

test5 = [[1, 1, 0], [2, 2, 0], [0, 0, 0]]

# circle
test2 = [[1, 0, 0], [-1, 0, 0], [0, 1, 0], [0, -1, 0]]

# sphere
test3 = [[1, 0, 0], [-1, 0, 0], [0, 1, 0], [0, -1, 0], [0, 0, 1], [0, 0, -1]]

test6 = [[0,0,0],[1,1,1]]
# one set of real test
test_real_1 = [[-9.4,  17.0,  -5.9], [-6.5, 17.7,  -8.2],
               [-5.0,  15.7, -11.1], [-2.1, 14.6, -8.9]]


linearity, flatness = inertia_tensor_transform(test6)

print(linearity, flatness)

#    # initilize max min mid
#     max = eigen_value[0]
#     max_index = 0
#     min = eigen_value[0]
#     min_index = 0
#     mid_index = 0

#     # sort the order
#     for i in range(1, 3, 1):
#         if max < eigen_value[i]:
#             max = eigen_value[i]
#             max_index = i
#         if min > eigen_value[i]:
#             min = eigen_value[i]
#             min_index = i

#     # solve three eigenvalue are equal
#     if min_index == max_index:
#         min_index = int(0)
#         mid_index = int(1)
#         max_index = int(2)
#     else:
#         mid_index = int(3 - max_index - min_index)

#     # obtain the min,mid,max eigen value
#     min_eigenvalue = eigen_value[min_index]
#     mid_eigenvalue = eigen_value[mid_index]
#     max_eigenvalue = eigen_value[max_index]

#     # obtain the principal Axes coordinate vector
#     x_prin = eigen_vector[min_index]  # x for largest extent
#     z_prin = eigen_vector[max_index]  # z for smallest extent
#     y_prin = eigen_vector[mid_index]  # y for middle extent

#     # initialize E_x E_y E_z
#     E_x = .0
#     E_y = .0
#     E_z = .0

#     # the inner prodcuet of (Atom_co * New_E_(xyz))
#     for i in range(0, length_seg, 1):
#         x_co = float(motion_xyz[i][0])
#         y_co = float(motion_xyz[i][1])
#         z_co = float(motion_xyz[i][2])

#         coordinate_vector = numpy.array([x_co, y_co, z_co])
#         x_prin = numpy.array(x_prin)
#         y_prin = numpy.array(y_prin)
#         z_prin = numpy.array(z_prin)

#         x_dot = numpy.inner(coordinate_vector, x_prin)
#         y_dot = numpy.inner(coordinate_vector, y_prin)
#         z_dot = numpy.inner(coordinate_vector, z_prin)

#         E_x += abs(x_dot) / length_seg
#         E_y += abs(y_dot) / length_seg
#         E_z += abs(z_dot) / length_seg

#     # If meet eigenvalue as same value
#     # Re_sort the E_x, E_y, E_z
#     if min_eigenvalue == mid_eigenvalue:
#         if E_x < E_y:
#             real_Ex = E_y
#             real_Ey = E_x
#             E_x = real_Ex
#             E_y = real_Ey
#     if mid_eigenvalue == max_eigenvalue:
#         if E_y < E_z:
#             real_Ey = E_z
#             real_Ez = E_y
#             E_y = real_Ey
#             E_z = real_Ez

#     # print(motion_xyz)
#     # print(eigen_value)
#     # print(eigen_vector)
#     # print(min_eigenvalue, mid_eigenvalue, max_eigenvalue)
#     # print(E_x, E_y, E_z)

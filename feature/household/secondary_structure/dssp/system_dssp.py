#! /usr/bin/python3

import os

#### iterating the pdb folder
pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/"
dssp_directory = "/data/Dropbox/project/HD/feature/ss/dssp_out/"

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        file_path = subdir + file

        pdbid = ''
        for i in range(0, 4, 1):
            pdbid += file[i]
        
        dssp_file_path = dssp_directory + pdbid + ".dssp"
        
        command_line = "mkdssp -i " + file_path +\
                       " -o " + dssp_file_path
        os.system(command_line)

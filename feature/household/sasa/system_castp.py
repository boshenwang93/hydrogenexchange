#! /usr/bin/python3

import os

# iterating the pdb folder
pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/"

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        file_path = subdir + file
        command_line = "newcast -fq4 . " + file
        os.system(command_line)

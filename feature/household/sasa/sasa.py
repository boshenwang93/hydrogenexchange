#! /usr/bin/python3
import re
import os

################################################################################
####### side chain fully exposed in AcGG-X-GG peptide ##########################
#####     DOI: 10.1021/bi9600153 Wimley, William C., Trevor P. Creamer, ########
################################################################################

# The total fully exposed side-chain sasa
dic_residue_sasa_sidechain = {
    "ALA": 80.0,
    "ARG": 216.0,
    "ASN": 131.0,
    "ASP": 123.9,
    "CYS": 108.4,
    "GLN": 159.4,
    "GLU": 152.0,
    "GLY": 43.3,
    "HIS": 164.3,
    "ILE": 158.1,
    "LEU": 159.3,
    "LYS": 188.3,
    "MET": 166.9,
    "PHE": 187.0,
    "PRO": 119.8,
    "SER": 93.2,
    "THR": 119.9,
    "TRP": 228.5,
    "TYR": 203.1,
    "VAL": 133.1,
}

####################################################################
#### Max Theoretical SASA  for amino acids #########################
#### With backbone #################################################
####  PMID 24278298 ################################################
####################################################################

dic_residue_sasa_max = {
    "ALA": 129.0,
    "ARG": 274.0,
    "ASN": 195.0,
    "ASP": 193.0,
    "CYS": 167.0,
    "GLN": 225.0,
    "GLU": 223.0,
    "GLY": 104.0,
    "HIS": 224.0,
    "ILE": 197.0,
    "LEU": 201.0,
    "LYS": 236.0,
    "MET": 224.0,
    "PHE": 240.0,
    "PRO": 159.0,
    "SER": 155.0,
    "THR": 172.0,
    "TRP": 285.0,
    "TYR": 263.0,
    "VAL": 174.0,
}

###################################################
#### it will return the absolute value of sasa ####
#### And exposed fraction #########################
###################################################


def read_castp_output(castp_output_file):
    list_residue_sasa = []

    f = open(castp_output_file, "r")

    list_unique_residue = []
    list_atom_entry = []
    for line in f:
        if line.startswith("ATOM"):
            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # atom's sasa
            atom_sasa = ""
            for i in range(39, 46, 1):
                atom_sasa += line[i]
            atom_sasa = atom_sasa.strip()

            residue_id = residue + "," + residue_seq_number + "," + chain

            atom_entry = residue_id + "," + atom + "," + atom_sasa
            list_atom_entry.append(atom_entry)

            if residue_id not in list_unique_residue:
                list_unique_residue.append(residue_id)

    for residue in list_unique_residue:
        residue_name = re.split(",", residue)[0]
        residue_name = residue_name.strip()
        residue_full_sasa = dic_residue_sasa_max[residue_name]
        residue_full_sasa = float(
            residue_full_sasa
        )  ## fully exposed side chain sasa, not including backbone

        residue_sasa = 0
        for atom_entry in list_atom_entry:
            atom_entry_id = re.split(",", atom_entry)[0] + "," + re.split(
                ",", atom_entry)[1] + "," + re.split(",", atom_entry)[2]

            if residue == atom_entry_id:
                atom_sasa = float(re.split(",", atom_entry)[4])
                residue_sasa += atom_sasa

        exposed_fraction = residue_sasa / residue_full_sasa

        ## keep 4 decimal
        residue_sasa = "%.4f" % residue_sasa
        exposed_fraction = "%.4f" % exposed_fraction
        output = residue + "," + str(residue_sasa) + "," + str(exposed_fraction)

        if output not in list_residue_sasa:
            list_residue_sasa.append(output)

    f.close()

    return list_residue_sasa

# catsp_sample_file = "castp_sample"
# for e in read_castp_output(catsp_sample_file):
#     print(e)

## iterating the catsp folder ########
castp_out_directory = "/data/Dropbox/project/HD/feature/sasa/castp_out/"

for subdir, dirs, files in os.walk(castp_out_directory):
    for file in files:
        file_path = subdir + file

        pdbid = ''
        for i in range(0, 4, 1):
            pdbid += file[i]

        list_residue_sasa = read_castp_output(file_path)

        for element in list_residue_sasa:
            element = pdbid + "," + element
            print(element)
# output format:
# PDB + residue_triple + residue_seq_number + chain_ID + SASA + exposed_fraction
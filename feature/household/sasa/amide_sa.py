 #! /usr/bin/python3
import os

###################################################
#### Written by Boshen Wang #######################
#### Input the castp output file ##################
#### Return value of sasa of backbone amide(N) ####
###################################################


# define function to output the amide_SA as list
def amide_sa_castp_output(castp_output_file):
    list_amide_sasa = []

    f = open(castp_output_file, "r")

    for line in f:
        if line.startswith("ATOM"):
            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # atom's sasa
            atom_sasa = ""
            for i in range(39, 46, 1):
                atom_sasa += line[i]
            atom_sasa = atom_sasa.strip()
            # keep 4 digit after dot
            atom_sasa = "%.4f" % (abs(float(atom_sasa)))

            out_entry = residue + "," + residue_seq_number + "," + chain + \
                "," + atom + "," + str(atom_sasa)

            if atom == "N":
                list_amide_sasa.append(out_entry)
            elif atom == "n":
                list_amide_sasa.append(out_entry)

    f.close()

    return list_amide_sasa


# catsp_sample_file = "castp_sample"
# for e in amide_sa_castp_output(catsp_sample_file):
#     print(e)

# iterating the catsp folder
castp_out_directory = "/data/Dropbox/project/HD/feature/sasa/castp_out/"

for subdir, dirs, files in os.walk(castp_out_directory):
    for file in files:
        file_path = subdir + file

        pdbid = ''
        for i in range(0, 4, 1):
            pdbid += file[i]

        list_residue_sasa = amide_sa_castp_output(file_path)

        for element in list_residue_sasa:
            element = pdbid + "," + element
            print(element)

# output format:
## PDB + residue_triple + residue_seq_number + chain_ID + N + N_SASA

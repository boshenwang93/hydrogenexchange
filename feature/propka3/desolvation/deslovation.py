#! /usr/bin/python3

#! /usr/bin/python3
import re
import math
import os

###################################################
#### Calculate Desolvation Effect for PDB #########
#### PMID: 26596171 ###############################
#### Written by Boshen Wang #######################
###################################################


###################################################
#### Description ##################################
# Function:

# the dictionary residue => heavy atom including terminal OXT
dic_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "CB", "OXT"),
    "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1", "OXT"),
    "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2", "OXT"),
    "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2", "OXT"),
    "CYS": ("N", "CA", "C", "O", "CB", "SG", "OXT"),
    "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2", "OXT"),
    "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2", "OXT"),
    "GLY": ("N", "CA", "C", "O", "OXT"),
    "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2", "OXT"),
    "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1", "OXT"),
    "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "OXT"),
    "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ", "OXT"),
    "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE", "OXT"),
    "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ", "OXT"),
    "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD", "OXT"),
    "SER": ("N", "CA", "C", "O", "CB", "OG", "OXT"),
    "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2"),
    "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2", "OXT"),
    "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH", "OXT"),
    "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "OXT"),
}

# read the PDB file
# return list of
# 1. all residue info
# 2. all heavy atom info
# 3. charged residue charged center (functional group center)
# 4. charged residue

def capture_heavy_atom(PDB_file_path):
    # list for Heavy atom entry
    list_heavy_atom_entry = []

    # list for unique residue
    list_residue = []

    # list for charged center
    list_charged_center = []
    # list for charged residue
    list_charged_residue = []

    f = open(PDB_file_path, 'r')

    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip().upper()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip().upper()

            # chain identifier
            chain = line[21].upper()

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            residue_info = residue + "," +\
                residue_seq_number + "," +\
                chain

            if residue_info not in list_residue:
                list_residue.append(residue_info)

            if residue in ["ASP", "GLU", "HIS", "CYS", "TYR", "LYS", "ARG"]:
                if residue_info not in list_charged_residue:
                    list_charged_residue.append(residue_info)

            hatom_info = residue + "," +\
                residue_seq_number + "," +\
                chain + "," +\
                atom + "," +\
                x + "," + y + "," + z

            if atom in dic_residue_heavyatom[residue]:
                list_heavy_atom_entry.append(hatom_info)

    f.close()

    # capture the charged residue, c&n terminal will be added in next
    for single_charged_residue in list_charged_residue:
        c_residue_triLetter = re.split(",", single_charged_residue)[0].strip()
        c_residue_seqnum = re.split(",", single_charged_residue)[1].strip()
        c_residue_chain = re.split(",", single_charged_residue)[2].strip()

        charge_center_x = 0
        charge_center_y = 0
        charge_center_z = 0

        for every_heavy_atom in list_heavy_atom_entry:
            hatom_residue_triLetter = re.split(
                ",", every_heavy_atom)[0].strip()
            hatom_residue_seqnum = re.split(",", every_heavy_atom)[1].strip()
            hatom_residue_chain = re.split(",", every_heavy_atom)[2].strip()
            hatom_atom_name = re.split(",", every_heavy_atom)[3].strip()
            hatom_x = re.split(",", every_heavy_atom)[4].strip()
            hatom_y = re.split(",", every_heavy_atom)[5].strip()
            hatom_z = re.split(",", every_heavy_atom)[6].strip()

            hatom_residue_info = hatom_residue_triLetter + "," + \
                hatom_residue_seqnum + "," + hatom_residue_chain
            if hatom_residue_info == single_charged_residue:

                if c_residue_triLetter == "ASP":
                    if hatom_atom_name in ["OD1", "OD2"]:
                        charge_center_x += float(hatom_x) / 2
                        charge_center_y += float(hatom_y) / 2
                        charge_center_z += float(hatom_z) / 2

                elif c_residue_triLetter == "GLU":
                    if hatom_atom_name in ["OE1", "OE2"]:
                        charge_center_x += float(hatom_x) / 2
                        charge_center_y += float(hatom_y) / 2
                        charge_center_z += float(hatom_z) / 2

                elif c_residue_triLetter == "HIS":
                    if hatom_atom_name in ["CG", "ND1", "CD2", "CE1", "NE2"]:
                        charge_center_x += float(hatom_x) / 5
                        charge_center_y += float(hatom_y) / 5
                        charge_center_z += float(hatom_z) / 5

                elif c_residue_triLetter == "CYS":
                    if hatom_atom_name == "SG":
                        charge_center_x += float(hatom_x)
                        charge_center_y += float(hatom_y)
                        charge_center_z += float(hatom_z)

                elif c_residue_triLetter == "TYR":
                    if hatom_atom_name == "OH":
                        charge_center_x += float(hatom_x)
                        charge_center_y += float(hatom_y)
                        charge_center_z += float(hatom_z)

                elif c_residue_triLetter == "LYS":
                    if hatom_atom_name == "NZ":
                        charge_center_x += float(hatom_x)
                        charge_center_y += float(hatom_y)
                        charge_center_z += float(hatom_z)

                elif c_residue_triLetter == "ARG":
                    if hatom_atom_name == "CZ":
                        charge_center_x += float(hatom_x)
                        charge_center_y += float(hatom_y)
                        charge_center_z += float(hatom_z)

        # keep 3 decimals for corresponding pdb format
        charge_residue_center_info = single_charged_residue + "," +\
            str("%0.3f" % charge_center_x) + "," +\
            str("%0.3f" % charge_center_y) + "," +\
            str("%0.3f" % charge_center_z)
        list_charged_center.append(charge_residue_center_info)

    # check the terminal residue and add to charged residue list
    list_seqnum = []
    for residue in list_residue:
        seqnum = re.split(",", residue)[1].strip()
        seqnum = int(seqnum)
        if seqnum not in list_seqnum:
            list_seqnum.append(seqnum)

    max_seqnum = max(list_seqnum)
    min_seqnum = min(list_seqnum)

    begin_residue_info = ""
    begin_x = 0
    begin_y = 0
    begin_z = 0

    end_residue_info = ""
    end_x = 0
    end_y = 0
    end_z = 0

    for heavy_atom in list_heavy_atom_entry:
        hatom_residue_triLetter = re.split(",", heavy_atom)[0].strip()
        hatom_residue_seqnum = re.split(",", heavy_atom)[1].strip()
        hatom_residue_chain = re.split(",", heavy_atom)[2].strip()
        hatom_atom_name = re.split(",", heavy_atom)[3].strip()

        hatom_x = re.split(",", heavy_atom)[4].strip()
        hatom_y = re.split(",", heavy_atom)[5].strip()
        hatom_z = re.split(",", heavy_atom)[6].strip()

        if int(hatom_residue_seqnum) == int(min_seqnum):
            if hatom_atom_name == "N":
                begin_residue_info = "NTR" + "," + hatom_residue_seqnum + "," + hatom_residue_chain
                begin_x += float(hatom_x)
                begin_y += float(hatom_y)
                begin_z += float(hatom_z)
                if residue_info not in list_charged_residue:
                    list_charged_residue.append(residue_info)

        elif int(hatom_residue_seqnum) == int(max_seqnum):
            if hatom_atom_name in ["O", "OXT"]:
                end_residue_info = "CTR" + "," + hatom_residue_seqnum + "," + hatom_residue_chain
                end_x += float(hatom_x) / 2
                end_y += float(hatom_y) / 2
                end_z += float(hatom_z) / 2
                if residue_info not in list_charged_residue:
                    list_charged_residue.append(residue_info)

    begin_n_termi_info = begin_residue_info + "," + \
        str(begin_x) + "," + str(begin_y) + "," + str(begin_z)
    end_c_termi_info = end_residue_info + "," + \
        str("%0.3f" % end_x) + "," + str("%0.3f" %
                                         end_y) + "," + str("%0.3f" % end_z)

    list_charged_center.append(begin_n_termi_info)
    list_charged_center.append(end_c_termi_info)

    list_charged_residue.append(begin_residue_info)
    list_charged_residue.append(end_residue_info)

    return list_residue, list_heavy_atom_entry, list_charged_center, list_charged_residue



########################################################################
# According to PropKa empirical function
# calculate buried ratio for charged center
# return dictionary [residue_info] => buried_ratio


def calculate_buried_ratio(charged_group_center_list, heavy_atom_list):
    dic_residue_buried_ratio = {}

    for charged_center in charged_group_center_list:
        count_contact = 0
        buried_ratio = 0

        charged_center_info = re.split(",", charged_center)[0] + "," +\
            re.split(",", charged_center)[1] + "," +\
            re.split(",", charged_center)[2]
        host_x = float(re.split(",", charged_center)[3])
        host_y = float(re.split(",", charged_center)[4])
        host_z = float(re.split(",", charged_center)[5])

        for each_heavy_atom in heavy_atom_list:
            guest_x = float(re.split(",", each_heavy_atom)[4])
            guest_y = float(re.split(",", each_heavy_atom)[5])
            guest_z = float(re.split(",", each_heavy_atom)[6])

            distance = math.sqrt(math.pow((guest_x - host_x), 2) +
                                 math.pow((guest_y - host_y), 2) +
                                 math.pow((guest_z - host_z), 2))
            if distance < 15:
                count_contact += 1

        if count_contact < 280:
            buried_ratio += 0
        elif count_contact < 560:
            buried_ratio += (count_contact - 280) / 280
        else:
            buried_ratio += 1

        # print(charged_center_info, ",", "%0.3f" % buried_ratio)
        buried_ratio = "%0.3f" % buried_ratio
        dic_residue_buried_ratio[charged_center_info] = buried_ratio

    return dic_residue_buried_ratio


########################################################################
## Desolvation energy defined by PropKa3
# ## pKa_Desol_shift = c * V / r**4

def calculate_desolvation (heavy_atom_list, charged_group_center_list, residue_buried_ratio_dictionary):

    list_pKa_shift_desolv = []

    # VDW volume defined by PropKa3
    dic_VDW_V = {
        "C": 20.58,
        "C4": 38.79,
        "N": 15.60,
        "O": 14.71,
        "S": 24.43,
    }

    # c constant for surface & buried 
    c_surface = 3.375
    c_buried = 13.5 
    
    for charged_center in charged_group_center_list:
        pKashit_desolv = 0.0

        ## the residue info 
        charged_center_info = re.split(",", charged_center)[0] + "," +\
            re.split(",", charged_center)[1] + "," +\
            re.split(",", charged_center)[2]
        
        ## host residue ID
        host_residue_info = re.split(",", charged_center)[1] + "," +\
                    re.split(",", charged_center)[2]
                
        ## residue x, y, z
        host_x = float(re.split(",", charged_center)[3])
        host_y = float(re.split(",", charged_center)[4])
        host_z = float(re.split(",", charged_center)[5])

        Buried_Ratio = float(residue_buried_ratio_dictionary[charged_center_info])
        c = c_surface - (c_surface - c_buried) * Buried_Ratio

        # print(charged_center_info, Buried_Ratio, c)

        for each_heavy_atom in heavy_atom_list:
            ## seqnum + chain ID
            guest_residue_info = re.split(",", each_heavy_atom)[1] + "," +\
                              re.split(",", each_heavy_atom)[2]

            guest_x = float(re.split(",", each_heavy_atom)[4])
            guest_y = float(re.split(",", each_heavy_atom)[5])
            guest_z = float(re.split(",", each_heavy_atom)[6])

            ## atom type
            guest_atom = re.split(",", each_heavy_atom)[3] 

            ## Assign the effectice VDW volume 
            ## Attention. According to PropKa 3 code, it should relative Volume
            V = 0
            guest_atom = guest_atom.upper()
            if guest_atom in ["C", "CA"]:
                V += float( dic_VDW_V["C"] / dic_VDW_V["C"] )

            elif guest_atom in ["CB", "CG", "CD", "CD1", "CD2", "CG1", "CG2",
                                "CE", "CE1", "CE2","CE3","CH2", "CZ", "CZ2", "CZ3"]:
                V += float( dic_VDW_V["C4"] / dic_VDW_V["C"] )

            elif guest_atom in ["N", "NE", "NE1", "NE2", "NH1","NZ","ND2", "ND1" ]:
                V += float( dic_VDW_V["N"] / dic_VDW_V["C"])

            elif guest_atom in ["OXT", "O", "OG", "OG1", "OG2", "OE1", 
                                "OE2", "OD1", "OD2", "OH" ]:
                V += float( dic_VDW_V["O"] / dic_VDW_V["C"] )
                
            elif guest_atom in ["SG", "SD"]:
                V +=  float( dic_VDW_V["S"] / dic_VDW_V["C"] )

            distance = math.sqrt(math.pow((guest_x - host_x), 2) +
                                 math.pow((guest_y - host_y), 2) +
                                 math.pow((guest_z - host_z), 2))
            if host_residue_info != guest_residue_info:
                if distance >= 2.75:
                    guest_shift_desolv = c * V / math.pow(distance, 4)
                    pKashit_desolv += guest_shift_desolv
        # keep 4 floating             
        pKashit_desolv = "%0.4f" % pKashit_desolv
        output = charged_center_info + "," +  str(pKashit_desolv)
        list_pKa_shift_desolv.append(output)
    return list_pKa_shift_desolv
                

out = open("desolv_shift.csv","w")
# iterating the pdb folder
pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/"

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        file_path = subdir + file
        pdb_id = re.split(".pdb", file)[0]

        if file.endswith(".pdb"):
            try:
                list_residue, list_heavy_atom, list_charge_center, list_charge_residue  = capture_heavy_atom(file_path)
                dic_buried_ratio = calculate_buried_ratio( list_charge_center,  list_heavy_atom)
                desolv_shift_list = calculate_desolvation(list_heavy_atom, list_charge_center, dic_buried_ratio)

                for element in desolv_shift_list:
                    out_line = pdb_id + "," + element + "\n"
                    out.write(out_line)

            except:
                print(pdb_id) 
out.close()
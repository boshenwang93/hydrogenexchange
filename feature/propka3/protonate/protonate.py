#! /usr/bin/python3

import re
import numpy
import math
import os
###########################################
#### VSEPR Theory to protonate H atom #####
#### Written by Boshen Wang ###############
###########################################

###########################################
#### Structure of the Script ##############
#### RemoveHydrogen => obtain heavy atom ##
#### AddHydrogen => Arrange H atom VSEPR ##
#### dic_radius => VDW radius for atoms ###
#### dic_residue_heavtatom => residue's ###
#### heavy atoms ##########################
#### dic_atom_valence_type => atom's ######
#### valence type, sp1, sp2,etc ###########

# the dictionary residue => heavy atom including terminal OXT
dic_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "CB", "OXT"),
    "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1", "OXT"),
    "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2", "OXT"),
    "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2", "OXT"),
    "CYS": ("N", "CA", "C", "O", "CB", "SG", "OXT"),
    "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2", "OXT"),
    "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2", "OXT"),
    "GLY": ("N", "CA", "C", "O", "OXT"),
    "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2", "OXT"),
    "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1", "OXT"),
    "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "OXT"),
    "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ", "OXT"),
    "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE", "OXT"),
    "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ", "OXT"),
    "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD", "OXT"),
    "SER": ("N", "CA", "C", "O", "CB", "OG", "OXT"),
    "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2"),
    "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2", "OXT"),
    "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH", "OXT"),
    "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "OXT"),
}


# INDEX list_heavy_atom
# [0] -- residue name
# [1] residue sequence number
# [2] chain ID
# [3] atom name
# [4][5][6] x y z

# INDEX list_residue
# [0] => residue tri letter
# [1] => sequence number
# [2] => chain ID
def RemoveHydrogen(pdb_file_path):

    # list for heavy atom entry
    list_heavy_atom_entry = []

    # list for unique residue
    list_residue = []

    # list for oxygen atoms
    list_oxygen = []

    #list for amide atoms
    list_amide = []

    f = open(pdb_file_path, 'r')

    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            if atom in dic_residue_heavyatom[residue]:
                temp_heavy_atom_entry = residue + "," + residue_seq_number + \
                    "," + chain + "," + atom + "," + x + "," + y + "," + z
                list_heavy_atom_entry.append(temp_heavy_atom_entry)

                if atom in ["OXT", "O", "OG", "OG1", "OG2", "OE1", 
                            "OE2", "OD1", "OD2", "OH" ]:
                    list_oxygen.append(temp_heavy_atom_entry)
                elif atom == "N":
                    list_amide.append(temp_heavy_atom_entry)

            residue_info = residue + "," + residue_seq_number + "," + chain
            if residue_info not in list_residue:
                list_residue.append(residue_info)
    f.close()
    return list_heavy_atom_entry, list_residue, list_oxygen, list_amide

#### protonate the backbone amide, N-H parallel C=O bond within a single peptide bond
#### ith N-H parallel to (i-1)th C=O
def Protonate_Backbone_Amide(heavy_atom_list, residue_list):
    list_amide_hydrogen = []

    for residue in list_residue:
        residue = residue.strip()
        residue_name = re.split(",", residue)[0]
        residue_seqnum = int (re.split(",", residue)[1])
        residue_chain = re.split(",", residue)[2]
        
        ## The C=O on the left peptide of current residue
        residue_C_carbonyl_x = 0
        residue_C_carbonyl_y = 0
        residue_C_carbonyl_z = 0

        residue_O_carbonyl_x = 0
        residue_O_carbonyl_y = 0
        residue_O_carbonyl_z = 0
        
        # the backbone amide of current residue 
        residue_amide_x = 0
        residue_amide_y = 0
        residue_amide_z = 0

        for heavy_atom in heavy_atom_list:
            heavy_atom_name = re.split(",", heavy_atom)[3]

            heavy_atom_x = float(re.split(",", heavy_atom)[4])
            heavy_atom_y = float(re.split(",", heavy_atom)[5])
            heavy_atom_z = float(re.split(",", heavy_atom)[6])

            heavy_atom_residue_name  =    re.split(",", heavy_atom)[0]
            heavy_atom_seqnum =   int( re.split(",", heavy_atom)[1])
            heavy_atom_chain =    re.split(",", heavy_atom)[2]

            if  residue_chain ==  heavy_atom_chain:
                if residue_seqnum == heavy_atom_seqnum + 1:
                    if heavy_atom_name == "C":
                        residue_C_carbonyl_x += heavy_atom_x
                        residue_C_carbonyl_y += heavy_atom_y
                        residue_C_carbonyl_z += heavy_atom_z
                    elif heavy_atom_name == "O":
                        residue_O_carbonyl_x += heavy_atom_x
                        residue_O_carbonyl_y += heavy_atom_y
                        residue_O_carbonyl_z += heavy_atom_z
                elif residue_seqnum == heavy_atom_seqnum:
                    if heavy_atom_name == "N":
                        residue_amide_x += heavy_atom_x
                        residue_amide_y += heavy_atom_y
                        residue_amide_z += heavy_atom_z

        # distance between C-O x,y,z
        CO_dx = residue_C_carbonyl_x - residue_O_carbonyl_x
        CO_dy = residue_C_carbonyl_y - residue_O_carbonyl_y
        CO_dz = residue_C_carbonyl_z - residue_O_carbonyl_z

        CO_distance = math.sqrt ( math.pow(CO_dx,2) + \
                                  math.pow(CO_dy,2) + \
                                  math.pow(CO_dz,2) )
        ## assign the H for amide Except the 1st residue
        if CO_distance != 0:
            NH_dx = CO_dx / CO_distance
            NH_dy = CO_dy / CO_distance
            NH_dz = CO_dz / CO_distance

            amide_H_x = "%.3f"% (residue_amide_x + NH_dx)
            amide_H_y = "%.3f"% (residue_amide_y + NH_dy)
            amide_H_z = "%.3f"% (residue_amide_z + NH_dz)

            new_H_amide_info = residue + "," +\
                               "H" + "," + str(amide_H_x) + "," + str(amide_H_y) + "," +\
                               str(amide_H_z)
            ### check the C=0 N-H direction, PASS
            # C_info = residue + "," +\
            #                    "C" + "," + str(residue_C_carbonyl_x) + "," + str(residue_C_carbonyl_y) + "," +\
            #                    str(residue_C_carbonyl_z)
            # O_info = residue + "," +\
            #                    "O" + "," + str(residue_O_carbonyl_x) + "," + str(residue_O_carbonyl_y) + "," +\
            #                    str(residue_O_carbonyl_z)
            # N_info = residue + "," +\
            #                    "N" + "," + str(residue_amide_x) + "," + str(residue_amide_y) + "," +\
            #                    str(residue_amide_z)
            # print(C_info)
            # print(O_info)
            # print(N_info)
            # print(new_H_amide_info)
            list_amide_hydrogen.append(new_H_amide_info)

        ## assign the H for 1st residue
        # else:

    return list_amide_hydrogen

def Hydrogen_bond_strength(list_amide,  list_oxygen, list_backbone_amide_hydrogen):
    list_amide_hbond_info = []

    cutoff_hbond = 3.0

    for N_atom in list_amide:
        N_residue = re.split(",",N_atom)[0] + "," +\
                    re.split(",",N_atom)[1] + "," +\
                    re.split(",",N_atom)[2]

        N_x = float(re.split(",",N_atom)[4])
        N_y = float(re.split(",",N_atom)[5])
        N_z = float(re.split(",",N_atom)[6])

        number_hbond = 0
        hbond_strength_Mine = 0 
        hbond_strength_Propka = 0

        for O_atom in list_oxygen:
            O_x = float(re.split(",",O_atom)[4])
            O_y = float(re.split(",",O_atom)[5])
            O_z = float(re.split(",",O_atom)[6])

            for H_atom in list_backbone_amide_hydrogen:
                H_residue =  re.split(",",H_atom)[0] + "," +\
                             re.split(",",H_atom)[1] + "," +\
                             re.split(",",H_atom)[2]

                H_x = float(re.split(",",H_atom)[4])
                H_y = float(re.split(",",H_atom)[5])
                H_z = float(re.split(",",H_atom)[6])

                dist_OH = math.sqrt( math.pow( (O_x - H_x), 2) +\
                                     math.pow( (O_y - H_y), 2) +\
                                     math.pow( (O_z - H_z), 2))

                dist_NH = math.sqrt( math.pow( (N_x - H_x), 2) +\
                                     math.pow( (N_y - H_y), 2) +\
                                     math.pow( (N_z - H_z), 2))

                vector_NH = numpy.array([(-N_x + H_x), (-N_y + H_y), (-N_z + H_z)])
                vector_HO = numpy.array([(O_x - H_x), (O_y - H_y), (O_z - H_z)])
                temp = math.sqrt( numpy.dot(vector_NH,vector_NH) ) *  math.sqrt( numpy.dot(vector_HO,vector_HO) )
                cos_theta = numpy.dot(vector_NH,vector_HO) / temp

                if N_residue == H_residue:
                    if dist_OH < cutoff_hbond:
                        if cos_theta >=0 :
                            number_hbond += 1
                            hbond_strength_Mine += cos_theta / math.pow(dist_OH,2)

                            if dist_OH <= 2:
                                hbond_strength_Propka += 0.85 
                            elif 2 < dist_OH < 3:
                                hbond_strength_Propka += 0.85 * (dist_OH - 2) * cos_theta
                            else:
                                hbond_strength_Propka += 0 
                
        out_entry = N_residue + "," + str(number_hbond) + "," +\
                    str(hbond_strength_Mine) + "," +\
                    str(hbond_strength_Propka) 

        list_amide_hbond_info.append(out_entry)
        
    return list_amide_hbond_info


# test_pdb_file = "/home/bwang/project/hydrogenexchange/feature/test.pdb"
# list_heavyatom, list_residue, list_oxygen, list_amide = RemoveHydrogen(test_pdb_file)
# list_amide_H = Protonate_Backbone_Amide(list_heavyatom, list_residue)
# hbond_info = Hydrogen_bond_strength(list_amide, list_oxygen, list_amide_H)

##############################################
######## Iterate folder #####################
# iterating the pdb folder
out = open("hbond", "w")

pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/"

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        file_path = subdir + file
        pdb_id = re.split(".pdb", file)[0]

        if file.endswith(".pdb"):
            try:
                list_heavyatom, list_residue, list_oxygen, list_amide = RemoveHydrogen(file_path)
                list_amide_H = Protonate_Backbone_Amide(list_heavyatom, list_residue)
                hbond_info_list = Hydrogen_bond_strength(list_amide, list_oxygen, list_amide_H)
                for e in hbond_info_list:
                    out_line = pdb_id + "," + e + "\n"
                    out.write(out_line)
            except:
                print(pdb_id) 
out.close()

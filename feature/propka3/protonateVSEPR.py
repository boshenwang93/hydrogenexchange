#! /usr/bin/python3
import re
import numpy
import math


###########################################
#### VSEPR Theory to protonate H atom #####
#### Written by Boshen Wang ###############
###########################################

###########################################
#### Structure of the Script ##############
#### RemoveHydrogen => obtain heavy atom ##
#### AddHydrogen => Arrange H atom VSEPR ##
#### dic_radius => VDW radius for atoms ###
#### dic_residue_heavtatom => residue's ###
#### heavy atoms ##########################
#### dic_atom_valence_type => atom's ######
#### valence type, sp1, sp2,etc ###########

# the dictionary residue => heavy atom including terminal OXT
dic_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "CB", "OXT"),
    "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1", "OXT"),
    "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2", "OXT"),
    "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2", "OXT"),
    "CYS": ("N", "CA", "C", "O", "CB", "SG", "OXT"),
    "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2", "OXT"),
    "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2", "OXT"),
    "GLY": ("N", "CA", "C", "O", "OXT"),
    "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2", "OXT"),
    "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1", "OXT"),
    "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "OXT"),
    "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ", "OXT"),
    "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE", "OXT"),
    "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ", "OXT"),
    "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD", "OXT"),
    "SER": ("N", "CA", "C", "O", "CB", "OG", "OXT"),
    "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2"),
    "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2", "OXT"),
    "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH", "OXT"),
    "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "OXT"),
}


# INDEX list_heavy_atom
# [0] -- residue name
# [1] residue sequence number
# [2] chain ID
# [3] atom name
# [4][5][6] x y z
# INDEX list_residue
# [0] => residue tri letter
# [1] => sequence number
# [2] => chain ID
def RemoveHydrogen(pdb_file_path):

    # list for heavy atom entry
    list_heavy_atom_entry = []

    # list for unique residue
    list_residue = []

    f = open(pdb_file_path, 'r')

    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            if atom in dic_residue_heavyatom[residue]:
                temp_heavy_atom_entry = residue + "," + residue_seq_number + \
                    "," + chain + "," + atom + "," + x + "," + y + "," + z
                list_heavy_atom_entry.append(temp_heavy_atom_entry)

            residue_info = residue + "," + residue_seq_number + "," + chain
            if residue_info not in list_residue:
                list_residue.append(residue_info)
    f.close()
    return list_heavy_atom_entry, list_residue

# protonate the backbone amide, N-H parallel C-O bond, not C=O
# Pro & GLY needs doubts
def Protonate_Amide(heavy_atom_list, residue_list):

    for residue in list_residue:
        residue_info = residue.strip()

        residue_C_carbonyl_x = 0
        residue_C_carbonyl_y = 0
        residue_C_carbonyl_z = 0

        residue_O_carbonyl_x = 0
        residue_O_carbonyl_y = 0
        residue_O_carbonyl_z = 0

        residue_amide_x = 0
        residue_amide_y = 0
        residue_amide_z = 0

        for heavy_atom in heavy_atom_list:
            heavy_atom_affiliated_residue_info = re.split(",", heavy_atom)[0] + "," +\
                re.split(",", heavy_atom)[1] + "," +\
                re.split(",", heavy_atom)[2]
            heavy_atom_affiliated_residue_info = heavy_atom_affiliated_residue_info.strip()

            heavy_atom_name = re.split(",", heavy_atom)[3]

            heavy_atom_x = float(re.split(",", heavy_atom)[4])
            heavy_atom_y = float(re.split(",", heavy_atom)[5])
            heavy_atom_z = float(re.split(",", heavy_atom)[6])

            if heavy_atom_affiliated_residue_info == residue:
                if heavy_atom_name == "C":
                    residue_C_carbonyl_x += heavy_atom_x
                    residue_C_carbonyl_y += heavy_atom_y
                    residue_C_carbonyl_z += heavy_atom_z
                elif heavy_atom_name == "O":
                    residue_O_carbonyl_x += heavy_atom_x
                    residue_O_carbonyl_y += heavy_atom_y
                    residue_O_carbonyl_z += heavy_atom_z
                elif heavy_atom_name == "N":
                    residue_amide_x += heavy_atom_x
                    residue_amide_y += heavy_atom_y
                    residue_amide_z += heavy_atom_z
        # distance between C-O x,y,z
        CO_dx = residue_C_carbonyl_x - residue_O_carbonyl_x
        CO_dy = residue_C_carbonyl_y - residue_O_carbonyl_y
        CO_dz = residue_C_carbonyl_z - residue_O_carbonyl_z

        CO_distance = math.sqrt ( math.pow(CO_dx,2) + \
                                  math.pow(CO_dy,2) + \
                                  math.pow(CO_dz,2) )

        NH_dx = CO_dx / CO_distance
        NH_dy = CO_dy / CO_distance
        NH_dz = CO_dz / CO_distance

        amide_H_x = "%.3f"% (residue_amide_x - NH_dx)
        amide_H_y = "%.3f"% (residue_amide_y - NH_dy)
        amide_H_z = "%.3f"% (residue_amide_z - NH_dz)

        new_H_amide_info = residue_info + "," +\
                           "H" + "," + str(amide_H_x) + "," + str(amide_H_y) + "," +\
                           str(amide_H_z)
        heavy_atom_list.append(new_H_amide_info)
    atom_list_with_amideH = heavy_atom_list
    return atom_list_with_amideH



test_pdb_file = "/home/bwang/project/hydrogenexchange/feature/test.pdb"

list_heavyatom, list_residue = RemoveHydrogen(test_pdb_file)

atomlist = Protonate_Amide(list_heavyatom, list_residue)
for e in atomlist:
    print(e)

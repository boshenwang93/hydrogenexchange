#! /usr/bin/python3
import re
import math
import os 

###################################################
#### Calculate Coulomb Interaction for PDB ########
#### PMID: 26596171 ###############################
#### Written by Boshen Wang #######################
###################################################


###################################################
#### Description ##################################
# Function:

test_pdb_file = "/home/bwang/project/hydrogenexchange/feature/test.pdb"

dic_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "CB"),
    "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1"),
    "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2"),
    "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2"),
    "CYS": ("N", "CA", "C", "O", "CB", "SG"),
    "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2"),
    "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2"),
    "GLY": ("N", "CA", "C", "O"),
    "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2"),
    "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1"),
    "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2"),
    "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ"),
    "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE"),
    "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ"),
    "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD"),
    "SER": ("N", "CA", "C", "O", "CB", "OG"),
    "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2"),
    "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2"),
    "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH"),
    "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2"),
}


def capture_heavy_atom(PDB_file_path):
    # list for Heavy atom entry
    list_heavy_atom_entry = []
    # list for alpha carbon entry
    list_ca_atom_entry = []

    f = open(PDB_file_path, 'r')

    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip().upper()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip().upper()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip().upper()

            # chain identifier
            chain = line[21].upper()

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            hatom_info = residue + "," +\
                residue_seq_number + "," +\
                chain + "," +\
                atom + "," +\
                x + "," + y + "," + z

            if atom in dic_residue_heavyatom[residue]:
                list_heavy_atom_entry.append(hatom_info)

            if atom == "CA":
                list_ca_atom_entry.append(hatom_info)

    f.close()

    return list_ca_atom_entry, list_heavy_atom_entry


def calculate_buried_ratio(alphacarbon_list, heavy_atom_list):
    list_residue_contactprofile_buriedratio = []

    for alpha_carbon in alphacarbon_list:
        count_contact = 0
        buried_ratio = 0

        alpha_carbon_residue_info = re.split(",", alpha_carbon)[0] + "," +\
            re.split(",", alpha_carbon)[1] + "," +\
            re.split(",", alpha_carbon)[2]

        host_x = float(re.split(",", alpha_carbon)[4])
        host_y = float(re.split(",", alpha_carbon)[5])
        host_z = float(re.split(",", alpha_carbon)[6])

        for each_heavy_atom in heavy_atom_list:
            guest_x = float(re.split(",", each_heavy_atom)[4])
            guest_y = float(re.split(",", each_heavy_atom)[5])
            guest_z = float(re.split(",", each_heavy_atom)[6])

            distance = math.sqrt(math.pow((guest_x - host_x), 2) +
                                 math.pow((guest_y - host_y), 2) +
                                 math.pow((guest_z - host_z), 2))
            if distance < 15:
                count_contact += 1

        if count_contact < 280:
            buried_ratio += 0
        elif count_contact < 560:
            buried_ratio += (count_contact - 280) / 280
        else:
            buried_ratio += 1

        out = alpha_carbon_residue_info + "," + \
            str(count_contact) + "," + str("%.3f" % buried_ratio)
        list_residue_contactprofile_buriedratio.append(out)

    return list_residue_contactprofile_buriedratio

#################################################################
######### Iterate The Whole PDB file folder #####################
#################################################################

pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/"
output_file = "contact_buriedratio.csv"

out = open(output_file,'w')

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        if file.endswith("pdb"):
            file_path = pdb_directory + file
            pdb_id = re.split('.pdb', file)[0]

            ca_atom_list, heavy_atom_list = capture_heavy_atom(file_path)
            contact_buried_list = calculate_buried_ratio(ca_atom_list, heavy_atom_list)

            for element in contact_buried_list:
                out_entry = pdb_id + "," + element + "\n"
                out.write(out_entry)
out.close()
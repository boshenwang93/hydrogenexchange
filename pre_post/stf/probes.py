#! /usr/bin/python3

import os
import re
import xml.etree.ElementTree as ET


dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V",
}

dic_PMID_STFEXP = {
    'dataset1': [1, 2, 3, 4],
    'dataset2': [5, 6],
    'dataset3': [7, 8],
    'dataset4': [9, 10],
    'dataset5': [13, 14, 15],
    'dataset6': [16, 17, 18],
    'dataset7': [19, 20],
    'dataset8': [21, 22],
    'dataset9': [23],
    'dataset10': [24, 25, 26],
    'dataset11': [27, 28, 29],
    'dataset12': [30, 31, 32],
    'dataset13': [33, 34, 35],
    'dataset14': [36],
    'dataset15': [37, 38],
    'dataset16': [39, 40, 41],
    'dataset17': [42],
    'dataset18': [43, 44, 45],
    'dataset19': [46, 47],
    'dataset20': [48, 49, 50],
    'dataset21': [51, 52],
    'dataset22': [53, 54, 55],
    'dataset23': [56],
    'dataset24': [57, 58, 59],
    'dataset25': [60],
    'dataset26': [61, 62, 63],
    'dataset27': [64],
    'dataset28': [65, 66, 67],
    'dataset29': [68, 69],
    'dataset30': [70, 71, 72],
    'dataset31': [73, 74, 75],
    'dataset32': [76, 77, 78],
    'dataset33': [79],
    'dataset34': [80, 81],
    'dataset35': [82, 83],
    'dataset36': [84, 85, 86],
    'dataset37': [88],
    'dataset38': [89, 90],
    'dataset39': [91, 92],
    'dataset40': [93, 94, 95],
    'dataset41': [96, 97],
    'dataset42': [98, 99],
    'dataset43': [100, 101],
    'dataset44': [102],
    'dataset45': [103, 104],
    'dataset46': [105, 106, 107],
    'dataset47': [108, 109],
    'dataset48': [110],
    'dataset49': [111, 112, 113],
    'dataset50': [114, 115],
    'dataset51': [116, 117],
    'dataset52': [118],
    'dataset53': [119],
    'dataset54': [120, 121, 122],
    'dataset55': [123, 124, 125],
    'dataset56': [126, 127],
    'dataset57': [128],
    'dataset58': [129, 130],
    'dataset59': [131, 132, 133],
    'dataset60': [134, 135],
    'dataset61': [136],
    'dataset62': [137],
    'dataset63': [138, 139],
    'dataset64': [140, 141],
    'dataset65': [142, 143],
    'dataset66': [144],
    'dataset67': [145, 146],
    'dataset68': [147],
    'dataset69': [148, 149],
    'dataset70': [150, 151, 152],
    'dataset71': [153, 154, 155],
    'dataset72': [156, 157, 158],
    'dataset73': [159, 160, 161],
    'dataset74': [162, 163, 164],
    'dataset75': [165, 166],
    'dataset76': [167],
    'dataset77': [168],
    'dataset78': [169, 170],
    'dataset79': [171, 172],
    'dataset80': [173],
    'dataset81': [174],
    'dataset82': [175],
    'dataset83': [177, 178],
    'dataset84': [179],
    'dataset85': [180, 181],
    'dataset86': [182],
    'dataset87': [183],
    'dataset88': [184],
    'dataset89': [185],
    'dataset90': [186],
    'dataset91': [187, 188],
    'dataset92': [189],
    'dataset93': [190],
    'dataset94': [191],
    'dataset95': [192],
    'dataset96': [193],
    'dataset97': [194, 195, 196],
    'dataset98': [197, 198],
    'dataset99': [199, 200],
    'dataset100': [201, 202],
    'dataset101': [203],
    'dataset102': [204, 205],
    'dataset103': [206, 207],
    'dataset104': [208, 209],
}


list_dataset_in = [
    "dataset73,1yyx",
    "dataset22,1hce",
    "dataset100,1shg",
    "dataset69,1r2t",
    "dataset3,1omp",
    "dataset4,1omp",
    "dataset71,1osp",
    "dataset12,1am7",
    "dataset49,1hrc",
    "dataset50,1hrc",
    "dataset51,1hrc",
    "dataset52,1hrc",
    "dataset53,1hrc",
    "dataset5,1ymb",
    "dataset36,1f21",
    "dataset37,1f21",
    "dataset9,3npo",
    "dataset11,3npo",
    "dataset57,1e3y",
    "dataset58,1e3y",
    "dataset74,2crt",
    "dataset75,2crt",
    "dataset78,3ci2",
    "dataset90,1rbx",
    "dataset91,1rbx",
    "dataset79,1pga",
    "dataset80,1pga",
    "dataset94,1hfx",
    "dataset18,1coe",
    "dataset19,1coe",
    "dataset99,1hcv",
    "dataset27,9pcy",
    "dataset103,1jbh",
    "dataset47,1bks",
    "dataset48,1bks",
    "dataset31,1onc",
    "dataset34,1ado",
    "dataset35,1ado",
    "dataset62,2vil",
    "dataset63,2vil",
    "dataset72,1be9",
    "dataset33,1omu",
    "dataset70,1mzk",
    "dataset24,5dfr",
    "dataset25,5dfr",
    "dataset88,1bdd",
    "dataset89,1bdd",
    "dataset59,1hrh",
    "dataset101,1sr2",
    "dataset95,3b0o",
    "dataset96,3b0o",
    "dataset30,1lz1",
    "dataset65,1yob",
    "dataset66,1yob",
    "dataset102,1g68",
    "dataset43,1joo",
    "dataset44,1joo",
    "dataset82,1hel",
    "dataset83,1hel",
    "dataset1,2abd",
    "dataset2,2abd",
    "dataset86,2ptl",
    "dataset92,1ok0",
    "dataset93,1ok0",
    "dataset60,1ubq",
    "dataset61,1ubq",
    "dataset67,1qlp",
    "dataset68,1qlp",
    "dataset28,2eql",
    "dataset29,2eql",
    "dataset14,1hfz",
    "dataset15,1hfz",
    "dataset16,1hfz",
    "dataset17,1hfz",
    "dataset98,451c",
    "dataset45,2lzm",
    "dataset46,2lzm",
    "dataset39,1ygw",
    "dataset40,1ygw",
    "dataset41,1ygw",
    "dataset42,1ygw",
    "dataset54,1rg8",
    "dataset55,1rg8",
    "dataset104,1opa",
    "dataset21,1a64",
    "dataset84,1i1b",
    "dataset85,1i1b",
    "dataset6,1mbc",
    "dataset7,1mbc",
    "dataset8,1mbc",
    "dataset64,1bta",
    "dataset97,3chy",

]


def parse_pdb_res(STF_XML_file):
    list_probes = []
    file = STF_XML_file

    stf_id = re.split("/", STF_XML_file)[-1]
    stf_id = re.split(".xml", stf_id)[0]

    tree = ET.parse(file)
    root = tree.getroot()

    for protein in root.iter('protein'):
        try:
            pdb = protein.get('pdb_id')
        except:
            pdb = "No PDB"

        for experiment in protein.iter('experiment'):
            exp_id = experiment.get('id')

            dataset_id = ''
            for k, v in dic_PMID_STFEXP.items():
                list_expid_dataset = v
                if int(exp_id) in list_expid_dataset:
                    dataset_id += k

            for conditions in experiment.iter('conditions'):
                pH = conditions.get('pH')
                temperature = conditions.get('temperature')
                probes = conditions.get('probes')

                out1 = dataset_id + "," + pdb + "," + stf_id + "," + probes

                if out1 not in list_probes:
                    list_probes.append(out1)

    return list_probes


dir = "/data/Dropbox/project/HD/database/STF/raw/"

for subdir, dirs, files in os.walk(dir):
    for file in files:
        file_path = dir + file
        list_1 = parse_pdb_res(file_path)

        for element in list_1:
            element = element.strip()
            id = re.split(",",element)[0]  + "," + re.split(",",element)[1]
            if id in list_dataset_in:
                print(element)

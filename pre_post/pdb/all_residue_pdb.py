#! /usr/bin/python3
import os
import re

# print out all residues from pdb single conformation file


def all_residue(pdb_file_path):

    # list_residue
    list_residue = []

    f = open(pdb_file_path, 'r')
    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            residue_identify = residue + "," + residue_seq_number + "," + chain
            if residue_identify not in list_residue:
                list_residue.append(residue_identify)

    f.close()

    return list_residue

# pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_SingleConformation/"
pdb_directory = "/data/Dropbox/project/HD/feature/ss/loop_pdb/"

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        file_path = subdir + file

        pdb_id = ''
        for i in range(0,4,1):
            pdb_id += file[i]
        pdb_id = pdb_id.strip()


        list_residue = all_residue(file_path)
        for element in list_residue:
            out_line = pdb_id + "," + element
            print(out_line)
#! /usr/bin/python3
import re

file = "/home/bwang/project/hydrogenexchange/csv/all_residue_pdb.csv"

f = open(file,"r")
for line in f:
    pdb_id = re.split(",",line)[0].strip()
    chain = re.split(",",line)[3].strip()

    if pdb_id == "1yob":
        if chain == "A":
            line = line.strip()
            print(line)
    elif pdb_id == "1ado":
        if chain == "A":
            line = line.strip()
            print(line)
    elif pdb_id == "1r2t":
        if chain == "B":
            line = line.strip()
            print(line)
    elif pdb_id == "3b0o":
        if chain == "A":
            line = line.strip()
            print(line)
    elif pdb_id == "1bks":
        if chain == "A":
            line = line.strip()
            print(line)
    elif pdb_id == "1a64":
        if chain == "A":
            line = line.strip()
            print(line)
    elif pdb_id == "1a2p":
        if chain == "A":
            line = line.strip()
            print(line)
    elif pdb_id == "1rg8":
        if chain == "A":
            line = line.strip()
            print(line)
    elif pdb_id == "1hrh":
        if chain == "A":
            line = line.strip()
            print(line)
    elif pdb_id == "1hfz":
        if chain == "A":
            line = line.strip()
            print(line)
    elif pdb_id == "1opa":
        if chain == "A":
            line = line.strip()
            print(line)
    
    else:
        line = line.strip()
        print(line)
f.close()
#! /usr/bin/python3
import re

file_path = "/home/bwang/project/hydrogenexchange/csv/all_residue_pdb.csv"

f = open(file_path,"r")

list_pdb = []
list_chain = []

for line in f:
    pdb = re.split(",",line)[0]
    chain = re.split(",",line)[3].strip()

    chain_id = pdb + "," + chain

    if pdb not in list_pdb:
        list_pdb.append(pdb)
    
    if chain_id not in list_chain:
        list_chain.append(chain_id)
f.close()

for pdb_id in list_pdb:
    count = 0
    for chain_id in list_chain:
        chain_pdb = re.split(",",chain_id)[0]
        if chain_pdb == pdb_id:
            count += 1
    if count != 1:
        print(pdb_id,count)

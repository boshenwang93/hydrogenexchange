#! /usr/bin/python3
import re
import os 

#### extract assigned chain from polymer 
def monomer(pdb_file_path,chain_assigned):
    pdb_id = re.split("/",pdb_file_path)[-1]
    pdb_id = re.split(".pdb",pdb_id)[0]

    single_monomer_file = "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/" +\
                          pdb_id + ".pdb"

    f = open(pdb_file_path, 'r')

    out = open(single_monomer_file,"w")

    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # chain identifier
            chain = line[21]
            if chain == chain_assigned:
                out.write(line)

    f.close()
    out.close()

#### list of polymer which output Chain A
list_chain_A = [
    "1yob",
    "1ado",
    "3b0o",
    "1bks",
    "1a64",
    "1a2p",
    "1am7",
    "1rg8",
    "1hrh",
    "1be9",
    "1opa",
    "1hfz",
]


#### iterate the PDB_Single_Conformation folder
pdb_directory = "/data/Dropbox/project/HD/database/coordinate/PDB_SingleConformation/"

for subdir, dirs, files in os.walk(pdb_directory):
    for file in files:
        file_path = subdir + file
        pdb = re.split("/",file_path)[-1]
        pdb = re.split(".pdb",pdb)[0]

        if pdb == "1osp":
            monomer(file_path,"O")
        elif pdb == "1r2t":
            monomer(file_path,"B")
        elif pdb in list_chain_A:
            monomer(file_path,"A")
        else:
            copy_command = "cp \t" + file_path + "\t" +\
                           "/data/Dropbox/project/HD/database/coordinate/PDB_Single_Monomer/"
            os.system(copy_command)
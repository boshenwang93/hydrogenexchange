#! /usr/bin/python3
import os
import re

file_pdb_list = "/data/Dropbox/project/HD/database/stf_clean/exp_pdb.csv"

f = open(file_pdb_list,'r')

list_pdb = []
for line in f:
    pdb_id = re.split(',',line)[1]
    pdb_id = re.split('\n',pdb_id)[0]

    if pdb_id not in list_pdb:
        list_pdb.append(pdb_id)
        rcsb_link = "https://files.rcsb.org/download/" + pdb_id + ".pdb"
        command_text = "wget " + rcsb_link + " -P /data/Dropbox/project/HD/database/PDB/"

        os.system(command_text)

f.close()

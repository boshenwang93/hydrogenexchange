#! /usr/bin/python3
import re

condition_file = "/home/bwang/project/hydrogenexchange/csv/condition.csv"
residue_seqnum_pdb = "/home/bwang/project/hydrogenexchange/csv/residue_monomer_pdb.csv"
outfile = "full_residue.csv"
out = open(outfile,"w")

cond = open(condition_file,"r")
for line in cond:
    pdb_id = re.split(",",line)[0]
    dataset_id = re.split(",",line)[1]

    f = open(residue_seqnum_pdb,"r")
    for residue in f:
        pdb_f = re.split(",",residue)[0]
        if pdb_f == pdb_id:
            out_line = dataset_id + "," + residue
            out.write(out_line)
    f.close()
cond.close()

out.close()
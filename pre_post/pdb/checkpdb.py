#! /usr/bin/python3
import re
import os
########################################################
############ Check irregular atom entry in pdb #########
########################################################

# the dictionary residue_type[key] ==> heavy atoms[value]
# residue type as string, heavy atoms as tuples

dic_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "CB"),
    "ARG": ("N", "CA", "C", "O", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1"),
    "ASN": ("N", "CA", "C", "O", "CB", "CG", "OD1", "ND2"),
    "ASP": ("N", "CA", "C", "O", "CB", "CG", "OD1", "OD2"),
    "CYS": ("N", "CA", "C", "O", "CB", "SG"),
    "GLN": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "NE2"),
    "GLU": ("N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2"),
    "GLY": ("N", "CA", "C", "O"),
    "HIS": ("N", "CA", "C", "O", "CB", "CG", "ND1", "CD2", "CE1", "NE2"),
    "ILE": ("N", "CA", "C", "O", "CB", "CG1", "CG2", "CD1"),
    "LEU": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2"),
    "LYS": ("N", "CA", "C", "O", "CB", "CG", "CD", "CE", "NZ"),
    "MET": ("N", "CA", "C", "O", "CB", "CG", "SD", "CE"),
    "PHE": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ"),
    "PRO": ("N", "CA", "C", "O", "CB", "CG", "CD"),
    "SER": ("N", "CA", "C", "O", "CB", "OG"),
    "THR": ("N", "CA", "C", "O", "CB", "OG1", "CG2"),
    "TRP": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2"),
    "TYR": ("N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH"),
    "VAL": ("N", "CA", "C", "O", "CB", "CG1", "CG2"),
}

# the dictionary residue_type[key] ==> Hydrogen atoms[value]
# residue type as string, hydrogen atoms as tuples
dic_residue_hydrogen = {
    "ALA": ("H", "HA", "1HB", "2HB", "3HB"),
    "ARG": ("H", "HA", "1HB", "2HB", "1HG", "2HG", "1HD", "2HD", "HE", "1HH1", "2HH1", "1HH2", "2HH2"),
    "ASN": ("H", "HA", "1HB", "2HB", "1HD2", "2HD2"),
    "ASP": ("H", "HA", "1HB", "2HB"),
    "CYS": ("H", "HA", "1HB", "2HB", "HG"),
    "GLN": ("H", "HA", "1HB", "2HB", "1HG", "2HG", "1HE2", "2HE2"),
    "GLU": ("H", "HA", "1HB", "2HB", "1HG", "2HG"),
    "GLY": ("H", "1HA", "2HA"),
    "HIS": ("H", "HA", "1HB", "2HB", "HD2", "HD2", "HE1"),
    "ILE": ("H", "HA", "HB", "1HG1", "2HG1", "1HG2", "2HG2", "3HG2", "1HD1", "2HD1", "3HD1"),
    "LEU": ("H", "HA", "1HB", "2HB", "HG", "1HD1", "2HD1", "3HD1", "1HD2", "2HD2", "3HD2"),
    "LYS": ("H", "HA", "1HB", "2HB", "1HG", "2HG", "1HD", "2HD", "1HE", "2HE", "1HZ", "2HZ", "3HZ"),
    "MET": ("H", "HA", "1HB", "2HB", "1HG", "2HG", "1HE", "2HE", "3HE"),
    "PHE": ("H", "HA", "1HB", "2HB", "HD1", "HD2", "HE1", "HE2", "HZ"),
    "PRO": ("H2", "H1", "HA", "1HB", "2HB", "1HG", "2HG", "1HD", "2HD"),
    "SER": ("H", "HA", "1HB", "2HB", "HG"),
    "THR": ("H", "HA", "HB", "HG1", "1HG2", "2HG2", "3HG2"),
    "TRP": ("H", "HA", "1HB", "2HB", "HD1", "HE1", "HE3", "HZ2", "HZ3", "HH2"),
    "TYR": ("H", "HA", "1HB", "2HB", "HD1", "HD2", "HE1", "HE2", "HH"),
    "VAL": ("H", "HA", "HB", "1HG1", "2HG1", "3HG1", "1HG2", "2HG2", "3HG2"),
}

# check the pdb file
# 1. check multiple conformations
# 2. check heavy atom missing or duplication


def check_pdb(pdb_file_path):
    # initialize list for atom entry
    list_atom_entry = []

    # list for heavy atom entry
    list_heavy_atom_entry = []

    # list_residue
    list_residue = []

    f = open(pdb_file_path, 'r')
    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "MODEL":
            print("warning! Multiple models detected!")

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            residue_identify = residue + "," + residue_seq_number + "," + chain
            if residue_identify not in list_residue:
                list_residue.append(residue_identify)

            atom_entry = residue + "," + residue_seq_number + "," + chain + "," +\
                atom
            if atom_entry not in list_atom_entry:
                list_atom_entry.append(atom_entry)

    f.close()

    # check heavy atom missing/duplication

    for residue in list_residue:
        list_residue_atom = []

        residue_host_id = re.split(",", residue)[0] +\
            re.split(",", residue)[1] +\
            re.split(",", residue)[2]

        for atom in list_atom_entry:
            atom_guest_id = re.split(",", atom)[0] +\
                re.split(",", atom)[1] +\
                re.split(",", atom)[2]

            if atom_guest_id == residue_host_id:
                list_residue_atom.append(re.split(",", atom)[3])

        for heavy_atom in dic_residue_heavyatom[re.split(",", residue)[0]]:
            if heavy_atom not in list_residue_atom:
                print(residue, "missing", heavy_atom)


### iterating the catsp folder ########
# pdb_directory = "/data/Dropbox/project/HD/database/PDB/"

# for subdir, dirs, files in os.walk(pdb_directory):
#     for file in files:
#         file_path = subdir + file

#         print("###############################")
#         print(file_path)
#         check_pdb(file_path)


# check the pdb which contains multiple conformations
# return count of conformations
# check whether conformations contains same atoms

def check_multiple_conformations(pdb_path):
    number_conformation = 0
    list_unique_atom = []

    f = open(pdb_path, "r")
    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()
        
        if line_record_identify == "MODEL":
            number_conformation += 1

        if line_record_identify == "ATOM":
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            atom_identify = atom + "," + residue + "," + chain + "," + residue_seq_number
            if atom_identify not in list_unique_atom:
                list_unique_atom.append(atom_identify)
    f.close()

    for atom_info in list_unique_atom:
        atom_count = 0

        f = open(pdb_path, "r")
        for line in f:
            line_record_identify = ""
        # column 1-6 is the identfication for the line record
            for i in range(0, 6, 1):
                line_record_identify += line[i]
            line_record_identify = line_record_identify.strip()

            if line_record_identify == "ATOM":
                atom = ""
                for i in range(12, 16, 1):
                    atom += line[i]
                atom = atom.strip()

                residue = ""
                for i in range(17, 20, 1):
                    residue += line[i]
                residue = residue.strip()

                chain = line[21]

                residue_seq_number = ""
                for i in range(22, 26, 1):
                    residue_seq_number += line[i]
                residue_seq_number = residue_seq_number.strip()

                atom_identify = atom + "," + residue + "," + chain + "," + residue_seq_number

                if atom_identify == atom_info:
                    atom_count += 1
        f.close()

        if atom_count != number_conformation:
            print(atom_info, "Loss Conformation",
                  atom_count, number_conformation)
    return 0

multi_conf_pdb = [
    '2ptl.pdb',
    '1ygw.pdb',
    '1sr2.pdb',
    '1yyx.pdb',
    '1mzk.pdb',
    '3ci2.pdb',
    '1coe.pdb',
    '2vil.pdb',
    '1omu.pdb',
    '1jbh.pdb',
    '2abd.pdb',
    '9pcy.pdb'
]
# PDB file directly download from RCSB
PDB_path = "/data/Dropbox/project/HD/database/coordinate/PDB/"

for element in multi_conf_pdb:
    file_path = PDB_path + element
    print(file_path)
    check_multiple_conformations(file_path)
#! /usr/bin/python3
import re

exchange_seqnum_by_stf = "/home/bwang/project/hydrogenexchange/csv/exchange_divided_dataset.csv"

f = open(exchange_seqnum_by_stf,"r")

for line in f:

    pdb = re.split(",",line)[0]
    dataset_id = re.split(",",line)[1]
    stf_id = re.split(",",line)[2]
    residue = re.split(",",line)[3]
    seqnum = re.split(",",line)[4].strip()

    if pdb == "1sr2":
        seqnum = int(seqnum) + 774
        out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
        out_line = out_line.strip()
        print(out_line)
    elif pdb == "1hrh":
        seqnum = int(seqnum) + 426
        out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
        out_line = out_line.strip()
        print(out_line)
    elif pdb == "1mzk":
        seqnum = int(seqnum) + 179
        out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
        out_line = out_line.strip()
        print(out_line)
    elif pdb == "3chy":
        seqnum = int(seqnum) + 1
        out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
        out_line = out_line.strip()
        print(out_line)
    elif pdb == "3ci2":
        seqnum = int(seqnum) + 17
        out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
        out_line = out_line.strip()
        print(out_line)
    elif pdb == "1jbh":
        seqnum = int(seqnum) - 1
        out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
        out_line = out_line.strip()
        print(out_line)
    elif pdb =="1opa":
        seqnum = int(seqnum) - 1
        out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
        out_line = out_line.strip()
        print(out_line)
    elif pdb == "1be9":
        seqnum = int(seqnum) + 296
        out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
        out_line = out_line.strip()
        print(out_line)
    elif pdb == "1hfz":
        seqnum = int(seqnum) 
        seqnum  -= 1
        out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
        out_line = out_line.strip()
        print(out_line)
    elif pdb == "1osp":
        seqnum = int(seqnum) 
        seqnum  += 17
        out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
        out_line = out_line.strip()
        print(out_line)

    elif pdb == "1e3y":
        if dataset_id == "dataset57":
            seqnum = int(seqnum) 
            seqnum  += 88
            out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
            out_line = out_line.strip()
            print(out_line)
        else:
            line = line.strip()
            print(line)

    elif pdb == "1rg8":
        if dataset_id == "dataset54":
            seqnum = int(seqnum) 
            seqnum  -= 14
            out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
            out_line = out_line.strip()
            print(out_line)
        elif dataset_id == "dataset56":
            seqnum = int(seqnum) 
            seqnum  -= 14
            out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
            out_line = out_line.strip()
            print(out_line)
        else:
            line = line.strip()
            print(line)

    elif pdb == "1hcv":
        seqnum = int(seqnum)
        if 59 < seqnum < 84:
            seqnum  -= 1
            out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
            out_line = out_line.strip()
            print(out_line)
        elif seqnum >= 84:
            seqnum  -= 4
            out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
            out_line = out_line.strip()
            print(out_line)
        else:
            line = line.strip()
            print(line)

    elif pdb == "1g68":
        seqnum = int(seqnum)
        if  seqnum < 37 :
            seqnum  += 21
            out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
            out_line = out_line.strip()
            print(out_line)
        elif 227 >= seqnum >= 37:
            seqnum  += 22
            out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
            out_line = out_line.strip()
            print(out_line)
        else:
            seqnum  += 24
            out_line = pdb + "," + dataset_id + "," + stf_id + "," +  residue + "," + str(seqnum)
            out_line = out_line.strip()
            print(out_line)

    else:
        line = line.strip()
        print(line)

f.close()
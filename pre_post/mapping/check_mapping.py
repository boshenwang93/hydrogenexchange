#! /usr/bin/python3
import re

#### check the mapping 

stf_file = "/home/bwang/project/hydrogenexchange/csv/exchange_pdb_seqnum.csv"

dssp_file = "/home/bwang/project/hydrogenexchange/pre_post/pdb/residue_monomer_pdb.csv"

list_pdb = []
list_exchange_stf = []

f1 = open(stf_file,"r")
for line in f1:
    pdb_id = re.split(",",line)[0]
    res_one = re.split(",",line)[3]
    seq_num = re.split(",",line)[4].strip()

    out = pdb_id + "," + res_one + "," + seq_num
    if out not in list_exchange_stf:
        list_exchange_stf.append(out)
    
    if pdb_id not in list_pdb:
        list_pdb.append(pdb_id)
f1.close()


list_pdb_residue_identify = []
f2 = open(dssp_file,"r")
for line in f2:
    pdb_id = re.split(",",line)[0]
    res_one = re.split(",",line)[1]
    seq_num = re.split(",",line)[2]
    chain = re.split(",",line)[3].strip()

    pdb_identify = pdb_id + "," + res_one + "," + seq_num + "," + chain
    if pdb_identify not in list_pdb_residue_identify:
        list_pdb_residue_identify.append(pdb_identify)
f2.close()

list_match_exchange = []
for exchange_residue in list_exchange_stf:
    for residue in list_pdb_residue_identify:
        if residue.startswith(exchange_residue):
            if exchange_residue not in list_match_exchange:
                list_match_exchange.append(exchange_residue)



for e in list_exchange_stf:
    if e not in list_match_exchange:
        print(e)


# none_map_file = "update3_not_mapped_residue"

# for pdb in list_pdb:
#     exchange_residue_no = 0 

#     f_ex = open(stf_file,"r")
#     for line in f_ex:
#         pdb_id = re.split(",",line)[0].strip()
#         if pdb_id == pdb:
#             exchange_residue_no += 1

#     f_ex.close()

#     no_map_residue_no = 0

#     f_nm = open(none_map_file,"r")
#     for line in f_nm:
#         pdb_id = re.split(",",line)[0].strip()
#         if pdb_id == pdb:
#             no_map_residue_no += 1
#     f_nm.close()

#     print(pdb,",",exchange_residue_no,",",no_map_residue_no)


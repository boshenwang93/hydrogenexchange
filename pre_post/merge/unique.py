#!/usr/bin/python3
import re

file = "full_dataset.csv"
list_unique = []

f = open(file,"r")

for line in f:
    line = line.strip()
    resid = re.split(",", line)[0] + "," +\
            re.split(",", line)[1] + "," +\
            re.split(",", line)[2] + "," +\
            re.split(",", line)[3] + "," +\
            re.split(",", line)[4]
    resid = resid.strip()

    if resid not in list_unique:
        print(line)

f.close()
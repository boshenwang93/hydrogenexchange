#! /usr/bin/python3

import re

cond = "/home/bwang/project/hydrogenexchange/csv/condition.csv"

dic_cond = {}
f_fe = open(cond, "r")
for line in f_fe:
    line = line.strip()
    dataset_id = re.split(",",line)[1]
    cond = re.split(",",line)[3] + "," + re.split(",",line)[4]
    dic_cond[dataset_id] = cond

f_fe.close()

f_full = open("/home/bwang/project/hydrogenexchange/pre_post/merge/full_ex.csv", "r")
for line in f_full:
    line = line.strip()
    full_id = re.split(",", line)[0]
    full_id = full_id.strip()
    out_line = line + "," + dic_cond[full_id]
    print(out_line)


f_full.close()
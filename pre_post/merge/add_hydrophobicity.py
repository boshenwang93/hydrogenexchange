#! /usr/bin/python3
import re

###############################################
###############################################
####### Hydrophobicity of residue #############
###############################################
###############################################

############################################################
##Wimley WC & White SH (1996). Nature Struct. Biol. 3:842-848.
## Wimley WC, Creamer TP & White SH (1996). Biochemistry 35:5109-5124
## White SH. & Wimley WC (1998). Biochim. Biophys. Acta 1376:339-352.
## White SH & Wimley WC (1999). Annu. Rev. Biophys. Biomol. Struc. 28:319-365.
#############################################################

dic_residue_hydrophobicity_interface = {
    "ALA": 0.17,
    "ARG": 0.81,
    "ASN": 0.42,
    "ASP-": 1.23,
    "ASP": -0.07,
    "CYS": -0.24,
    "GLN": 0.58,
    "GLU-": 2.02,
    "GLU": -0.01,
    "GLY": 0.01,
    "HIS+": 0.96,
    "HIS": 0.17,
    "ILE": -0.31,
    "LEU": -0.56,
    "LYS": 0.99,
    "MET": -0.23,
    "PHE": -1.13,
    "PRO": 0.45,
    "SER": 0.13,
    "THR": 0.14,
    "TRP": -1.85,
    "TYR": -0.94,
    "VAL": 0.07,
}

dic_residue_hydrophobicity_octanol = {
    "ALA": 0.50,
    "ARG": 1.81,
    "ASN": 0.85,
    "ASP-": 3.64,
    "ASP": 0.43,
    "CYS": -0.02,
    "GLN": 0.77,
    "GLU-": 3.63,
    "GLU": 0.11,
    "GLY": 1.15,
    "HIS+": 2.33,
    "HIS": 0.11,
    "ILE": -1.12,
    "LEU": -1.25,
    "LYS": 2.80,
    "MET": -0.67,
    "PHE": -1.71,
    "PRO": 0.14,
    "SER": 0.46,
    "THR": 0.25,
    "TRP": -2.09,
    "TYR": -0.71,
    "VAL": -0.46,
}

dic_residue_hydrophobicity_octanolinterface = {
    "ALA": 0.33,
    "ARG": 1.00,
    "ASN": 0.43,
    "ASP-": 2.41,
    "ASP": 0.50,
    "CYS": 0.22,
    "GLN": 0.19,
    "GLU-": 1.61,
    "GLU": 0.12,
    "GLY": 1.14,
    "HIS+": 1.37,
    "HIS": -0.06,
    "ILE": -0.81,
    "LEU": -0.69,
    "LYS": 1.81,
    "MET": -0.44,
    "PHE": -0.58,
    "PRO": -0.31,
    "SER": 0.33,
    "THR": 0.11,
    "TRP": -0.24,
    "TYR": 0.23,
    "VAL": -0.53,
}

file = "/home/bwang/project/hydrogenexchange/pre_post/full_ex_Nsa_SaFrac_Contact_hbond_ss_con.csv"

f = open(file,"r")
for line in f:
    line = line.strip()
    residue = re.split(",",line)[2]
    residue = residue.strip()
    out = line + "," + str(dic_residue_hydrophobicity_octanol[residue])
    print(out)

f.close()
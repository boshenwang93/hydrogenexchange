:: Converts latex to docx, works okay! Assumes pandoc is installed:
::  https://pandoc.org/
:: Script based on pandoc commands found here:
::  https://jabranham.com/blog/2016/11/using-pandoc-export-to-word/

@echo off
set script_dir=%~dp0
set tex_path=%script_dir%BW_HX.tex
set doc_path=%script_dir%BW_HX.docx
pandoc %tex_path% -S -o %doc_path%
echo Finished.


#!/bin/bash

# Path to script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
pushd ./
cd "$SCRIPT_DIR"
latex BW_HX.tex
dvipdfm BW_HX.dvi
popd

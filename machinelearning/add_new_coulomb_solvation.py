#! /usr/bin/python3
import re

# feature file path
file_feature = "/home/bwang/project/hydrogenexchange/machinelearning/new_solvation.csv"
dic_resid_feature = {}

f_feature = open(file_feature,"r")
for line in f_feature:
    line = line.strip()
    residue_id = re.split(",",line)[0] + "," +\
                 re.split(",",line)[1] + "," +\
                 re.split(",",line)[2]
    
    feature_value = re.split(",",line)[4]
    dic_resid_feature[residue_id] = feature_value
f_feature.close()


f_origin = "new_pos"

f = open(f_origin,"r")
for line in f:
    line = line.strip()
    residue_id = re.split(",",line)[1] + "," +\
                 re.split(",",line)[2] + "," +\
                 re.split(",",line)[3]
    try:    
        out_line = line  +  "," + dic_resid_feature[residue_id]
        print(out_line)
    except:
        out_line = line + ",0" 
        print(out_line)

f.close()
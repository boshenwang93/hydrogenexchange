## Input the data and randomly select test dataset
nd <-
  read.csv("/home/bwang/project/hydrogenexchange/machinelearning/negative.csv",
           header = TRUE)

pd <-
  read.csv("/home/bwang/project/hydrogenexchange/machinelearning/positive.csv",
           header = TRUE)

# Splits single class dataset into test and train sets
split_data <- function(class_data,
                       testsize = 500){
  class_data$exchange = as.factor(class_data$exchange)
  class_data$ss = as.factor(class_data$ss)
  class_data$residue = as.factor(class_data$residue)
  
  test_ind = sample(seq_len(nrow(class_data)), size = testsize)
  test_data = class_data[test_ind, ]
  train_data = class_data[-test_ind, ]
  
  return(list(test = test_data, train = train_data))
}

neg_split = split_data(nd)
pos_split  = split_data(pd)

testD = rbind(neg_split$test, pos_split$test)
trainD = rbind(neg_split$train, pos_split$train)

stratasize = min(nrow(pos_split$train), nrow(neg_split$train))

#### SVM
library(e1071)
hdsvm <-
  svm(
    formula = exchange ~ ph + temperature  + contact + ss  + hbondStrength  + sasa  + safraction +  N_sa+ residue ,
    data = trainD,
    strata = trainD$exchange,
    sampsize = c(stratasize,stratasize),
    cost = 100,
    gamma = 0.5
  )


pred <- predict(hdsvm, newdata = testD)

table(pred, testD$exchange)



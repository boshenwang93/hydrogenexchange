#! /usr/bin/python3

import re

list_unique = []
f = open("positive.csv","r")
for line in f:
    line = line.strip()
    dataset_id = re.split(",",line)[0]
    pdb = re.split(",",line)[1]

    out = dataset_id + "," + pdb
    if out not in list_unique:
        list_unique.append(out)
f.close()

for e in list_unique:
    out = '"' + e + '"' + ","
    print(out)

